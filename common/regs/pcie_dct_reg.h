/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only OR BSD-2-Clause
 *
 * Generated from pcie_regs.txt, git +
 */

#ifndef __PCIE_DCT__
#define __PCIE_DCT__

#define PCIE_MODULE_ID_REG 0x00000000
#define PCIE_CONFIG1_REG 0x00000004
#define PCIE_CONFIG2_REG 0x00000008
#define PCIE_CONFIG3_REG 0x0000000C
#define PCIE_STATUS1_REG 0x00000010
#define PCIE_STATUS2_REG 0x00000014
#define PCIE_STATUS3_REG 0x00000018
#define PCIE_MSI_IO_REG 0x0000001C
#define PCIE_MSI_VEC_REG 0x00000020
#define PCIE_EN_CLK_REG 0x00000024
#define PCIE_RESETS_REG 0x00000028
#define PCIE_MPLLA_REG 0x0000002C
#define PCIE_MPLLA_SSC1_REG 0x00000030
#define PCIE_MPLLA_SSC2_REG 0x00000034
#define PCIE_MPLLB_REG 0x00000038
#define PCIE_MPLLB_SSC1_REG 0x0000003C
#define PCIE_MPLLB_SSC2_REG 0x00000040
#define PCIE_TX_EQ_G1_REG 0x00000044
#define PCIE_TX_EQ_G2_REG 0x00000048
#define PCIE_TX_EQ_G3_REG 0x0000004C
#define PCIE_FW_RAM_CTRL_REG 0x00000050
#define PCIE_FW_RAM_STAT_REG 0x00000054
#define PCIE_PHY_EXTENDED_CFG_REG 0x00000058
#define PCIE_PHY_CR_CTRL_0_REG 0x00000100
#define PCIE_PHY_CR_CTRL_1_REG 0x0000010C
#define PCIE_PHY_CR_CTRL_2_REG 0x00000118
#define PCIE_PHY_CR_CTRL_3_REG 0x00000124
#define PCIE_PHY_CR_WR_DA_0_REG 0x00000104
#define PCIE_PHY_CR_WR_DA_1_REG 0x00000110
#define PCIE_PHY_CR_WR_DA_2_REG 0x0000011C
#define PCIE_PHY_CR_WR_DA_3_REG 0x00000128
#define PCIE_PHY_CR_RD_0_REG 0x00000108
#define PCIE_PHY_CR_RD_1_REG 0x00000114
#define PCIE_PHY_CR_RD_2_REG 0x00000120
#define PCIE_PHY_CR_RD_3_REG 0x0000012C
#define PCIE_ISM_REG 0x00000200
#define PCIE_RIS_REG 0x00000204
#define PCIE_MIS_REG 0x00000208
#define PCIE_ISC_REG 0x0000020C
#define PCIE_ISS_REG 0x00000210
#define PCIE_ERROR_ISM_REG 0x00000220
#define PCIE_ERROR_RIS_REG 0x00000224
#define PCIE_ERROR_MIS_REG 0x00000228
#define PCIE_ERROR_ISC_REG 0x0000022C
#define PCIE_ERROR_ISS_REG 0x00000230

/* PCIE_MODULE_ID_REG */
// Module written by DCT.
#define DCT_ID_MASK 0xFF000000U
#define DCT_ID_SHIFT 24U
// Project ZUKIMO.
#define PROJECT_ID_MASK 0x00FF0000U
#define PROJECT_ID_SHIFT 16U
// Revision.
#define REVISION_MASK 0x0000FF00U
#define REVISION_SHIFT 8U
// PCIe Module.
#define MODULE_ID_MASK 0x000000FFU
#define MODULE_ID_SHIFT 0U

/* PCIE_CONFIG1_REG */
// Controller device type:
// 0: End Point (EP)
// (device_type driven to 4'b0000)
// 1: Root Complex (RC)
// (device_type driven to 4'b0100)
// Only change during controller reset.
#define DEVICE_TYPE_MASK 0x00000001U
#define DEVICE_TYPE_BIT 0
// Enable Spread Spectrum Clock
// Only change during PHY reset.
#define PHY_MPLL_SSC_EN_MASK 0x00000002U
#define PHY_MPLL_SSC_EN_BIT 1
// External PCLK request
#define EXT_PCLK_REQ_MASK 0x00000004U
#define EXT_PCLK_REQ_BIT 2
// Power stable for Raw PCS
#define PHY_PCS_PWR_STABLE_MASK 0x00000078U
#define PHY_PCS_PWR_STABLE_SHIFT 3U
// Power stable for PMA
#define PHY_PMA_PWR_STABLE_MASK 0x00000780U
#define PHY_PMA_PWR_STABLE_SHIFT 7U
// Enable for high performance prescaler (documentation missing)
#define PHY_SUP_PRE_HP_MASK 0x00000800U
#define PHY_SUP_PRE_HP_BIT 11
// Parallel (RX to TX) loopback enable
#define PHY_RX2TX_PAR_LB_EN_MASK 0x00001000U
#define PHY_RX2TX_PAR_LB_EN_BIT 12
// Resistor tune request
#define PHY_RTUNE_REQ_MASK 0x00002000U
#define PHY_RTUNE_REQ_BIT 13
// Bypass equalization calculated values
#define PHY_TX_BYPASS_EQ_CALC_MASK 0x00004000U
#define PHY_TX_BYPASS_EQ_CALC_BIT 14
// Enable external overrides of the PHY configuration inputs.
#define PHY_EXT_CTRL_SEL_MASK 0x00008000U
#define PHY_EXT_CTRL_SEL_BIT 15
// RX termination enable for all lanes
#define PIPE_RX_TERMINATION_MASK 0x00010000U
#define PIPE_RX_TERMINATION_BIT 16
// Selects between legacy mode CDR logic and the new implementation
#define PIPE_RX_CDR_LEGACY_EN_MASK 0x00020000U
#define PIPE_RX_CDR_LEGACY_EN_BIT 17
// Enables re-locking when alignment is lost in non-8B10 mode.
#define PIPE_RX_RECAL_CONT_EN_MASK 0x00040000U
#define PIPE_RX_RECAL_CONT_EN_BIT 18
// Enable monitoring the PIPE interface signal pipe_lane_powerdown
// Any change must be followed by PHY reset.
#define UPCS_PIPE_CONFIG_MASK 0x00080000U
#define UPCS_PIPE_CONFIG_BIT 19
// Power stable to PCS
#define UPCS_PWR_STABLE_MASK 0x00100000U
#define UPCS_PWR_STABLE_BIT 20
// Provide a control to disable the CDR in PCIE GEN1 mode when there is electrical idle on rx line without EIOS
#define PIPE_RX_IDLE_LOS_CNT_MASK 0x07E00000U
#define PIPE_RX_IDLE_LOS_CNT_SHIFT 21U
// Frequency range of phy_clk_ref_p/m. Reset value corresponds to 100MHz.
#define PHY_MPLL_REF_RANGE_MASK 0xF8000000U
#define PHY_MPLL_REF_RANGE_SHIFT 27U

/* PCIE_CONFIG2_REG */
// Disable DBI writes to read-only registers:
// (driving app_dbi_ro_wr_disable)
#define APP_DBI_RO_WR_DISABLE_MASK 0x00000001U
#define APP_DBI_RO_WR_DISABLE_BIT 0
// enable test mode in the Register Checking Logic
#define CDM_REG_CHK_TEST_EN_MASK 0x00000002U
#define CDM_REG_CHK_TEST_EN_BIT 1
// Diagnostic Control Bus
// x01: Insert LCRC error by inverting the LSB of LCRC
// x10: Insert ECRC error by inverting the LSB of ECRC
// 1xx: Select Fast Link Mode
#define DIAG_CTRL_BUS_MASK 0x0000001CU
#define DIAG_CTRL_BUS_SHIFT 2U
// Performs manual lane reversal for receive lanes
#define RX_LANE_FLIP_EN_MASK 0x00000020U
#define RX_LANE_FLIP_EN_BIT 5
// Performs manual lane reversal for transmit lanes
#define TX_LANE_FLIP_EN_MASK 0x00000040U
#define TX_LANE_FLIP_EN_BIT 6
// Allow the LTSSM to continue link establishment.
// To be set after finishing reprogramming the controller configuration registers using the DBI,
#define APP_LTSSM_ENABLE_MASK 0x00000080U
#define APP_LTSSM_ENABLE_BIT 7

/* PCIE_CONFIG3_REG */
// Power state for lanes
#define PIPE_LANE_POWERDOWN_MASK 0x0000000FU
#define PIPE_LANE_POWERDOWN_SHIFT 0U
// TX-to-RX loopback enable for all lanes
#define PIPE_LANE_TX2RX_LOOPBK_MASK 0x00000010U
#define PIPE_LANE_TX2RX_LOOPBK_BIT 4
// RX equalization training mode enable for all lanes
#define PIPE_RX_EQ_TRAINING_MASK 0x00000020U
#define PIPE_RX_EQ_TRAINING_BIT 5

/* PCIE_STATUS1_REG */
// Internal PMA switch stable, 1 bit per lane
#define PHY_ANA_PWR_STABLE_MASK 0x0000000FU
#define PHY_ANA_PWR_STABLE_SHIFT 0U
// Resistor tune acknowledge. Indicates that a resistor tune has completed.
#define PHY_RTUNE_ACK_MASK 0x00000010U
#define PHY_RTUNE_ACK_BIT 4

/* PCIE_STATUS2_REG */
// Power state:
// 0000: P0 (L0): normal
// 0001: P0s (L0s): low recovery time, power saving.
// 0010: P1 (L1): longer recovery time, additional power saving.
// 0011: P2 (L2): lowest power state.
#define MAC_PHY_POWERDOWN_MASK 0x0000000FU
#define MAC_PHY_POWERDOWN_SHIFT 0U
// Indicates that flow control has been initiated and the Data link layer is ready to transmit and receive packets.
#define RDLH_LINK_UP_MASK 0x00000010U
#define RDLH_LINK_UP_BIT 4
// The current power management D-state:
// 000: D0
// 001: D1
// 010: D2
// 011: D3
// 100: Uninitialized
#define PM_DSTATE_MASK 0x000000E0U
#define PM_DSTATE_SHIFT 5U
// Indicates that L1 entry process is in progress.
#define PM_L1_ENTRY_STARTED_MASK 0x00000100U
#define PM_L1_ENTRY_STARTED_BIT 8
// Indicates that power management is in L0s state.
#define PM_LINKST_IN_L0S_MASK 0x00000200U
#define PM_LINKST_IN_L0S_BIT 9
// Indicates that power management is in L1 state.
#define PM_LINKST_IN_L1_MASK 0x00000400U
#define PM_LINKST_IN_L1_BIT 10
// Indicates that power management is in L2 state.
#define PM_LINKST_IN_L2_MASK 0x00000800U
#define PM_LINKST_IN_L2_BIT 11
// Indicates that power management is exiting L2 state.
#define PM_LINKST_L2_EXIT_MASK 0x00001000U
#define PM_LINKST_L2_EXIT_BIT 12
// Power management master FSM state.
#define PM_MASTER_STATE_MASK 0x0003E000U
#define PM_MASTER_STATE_SHIFT 13U
// Power management slave FSM state.
#define PM_SLAVE_STATE_MASK 0x007C0000U
#define PM_SLAVE_STATE_SHIFT 18U

/* PCIE_STATUS3_REG */
// The current link operating rate:
// 0: 2.5 GT/s
// 1: 5.0 GT/s
// 2: 8.0 GT/s
#define PM_CURRENT_DATA_RATE_MASK 0x00000007U
#define PM_CURRENT_DATA_RATE_SHIFT 0U
// PHY Link up/down indicator
#define SMLH_LINK_UP_MASK 0x00000008U
#define SMLH_LINK_UP_BIT 3
// Current state of the LTSSM:
// 0x00: S_DETECT_QUIET
// 0x01: S_DETECT_ACT
// 0x02: S_POLL_ACTIVE
// 0x03: S_POLL_COMPLIANCE
// 0x04: S_POLL_CONFIG
// 0x05: S_PRE_DETECT_QUIET
// 0x06: S_DETECT_WAIT
// 0x07: S_CFG_LINKWD_START
// 0x08: S_CFG_LINKWD_ACEPT
// 0x09: S_CFG_LANENUM_WAI
// 0x0A: S_CFG_LANENUM_ACEPT
// 0x0B: S_CFG_COMPLETE
// 0x0C: S_CFG_IDLE
// 0x0D: S_RCVRY_LOCK
// 0x0E: S_RCVRY_SPEED
// 0x0F: S_RCVRY_RCVRCFG
// 0x10: S_RCVRY_IDLE
// 0x11: S_L0
// 0x12: S_L0S
// 0x13: S_L123_SEND_EIDLE
// 0x14: S_L1_IDLE
// 0x15: S_L2_IDLE
// 0x16: S_L2_WAKE
// 0x17: S_DISABLED_ENTRY
// 0x18: S_DISABLED_IDLE
// 0x19: S_DISABLED
// 0x1A: S_LPBK_ENTRY
// 0x1B: S_LPBK_ACTIVE
// 0x1C: S_LPBK_EXIT
// 0x1D: S_LPBK_EXIT_TIMEOUT
// 0x1E: S_HOT_RESET_ENTRY
// 0x1F: S_HOT_RESET
// 0x20: S_RCVRY_EQ0
// 0x21: S_RCVRY_EQ1
// 0x22: S_RCVRY_EQ2
// 0x23: S_RCVRY_EQ3
#define SMLH_LTSSM_STATE_MASK 0x000003F0U
#define SMLH_LTSSM_STATE_SHIFT 4U
// Asserted during all Recovery Equalization states
#define SMLH_LTSSM_STATE_RCVRY_EQ_MASK 0x00000400U
#define SMLH_LTSSM_STATE_RCVRY_EQ_BIT 10

/* PCIE_MSI_IO_REG */
// MSI IO status
#define MSI_CTRL_IO_MASK 0xFFFFFFFFU
#define MSI_CTRL_IO_SHIFT 0U

/* PCIE_MSI_VEC_REG */
// MSI interrupt vector
#define MSI_CTRL_INT_VEC_MASK 0x000000FFU
#define MSI_CTRL_INT_VEC_SHIFT 0U

/* PCIE_EN_CLK_REG */
// clock enable
#define SW_EN_CLK_AXI_PCIE_TX_MASK 0x00000001U
#define SW_EN_CLK_AXI_PCIE_TX_BIT 0
// clock enable
#define SW_EN_CLK_AXI_PCIE_RX_MASK 0x00000002U
#define SW_EN_CLK_AXI_PCIE_RX_BIT 1
// clock enable
#define SW_EN_CLK_AXI_PCIE_DBI_MASK 0x00000004U
#define SW_EN_CLK_AXI_PCIE_DBI_BIT 2

/* PCIE_RESETS_REG */
// software reset, active low
#define SW_RESET_AXI_PCIE_TX_N_MASK 0x00000001U
#define SW_RESET_AXI_PCIE_TX_N_BIT 0
// software reset, active low
#define SW_RESET_AXI_PCIE_RX_N_MASK 0x00000002U
#define SW_RESET_AXI_PCIE_RX_N_BIT 1
// software reset, active low
#define SW_RESET_AXI_PCIE_DBI_N_MASK 0x00000004U
#define SW_RESET_AXI_PCIE_DBI_N_BIT 2
// software reset, active low
#define SW_RESET_PIPE_LANE_N_MASK 0x00000008U
#define SW_RESET_PIPE_LANE_N_BIT 3
// software reset, active low
#define SW_RESET_PHY_N_MASK 0x00000010U
#define SW_RESET_PHY_N_BIT 4
// software reset, active low
#define SW_RESET_CTL_N_MASK 0x00000020U
#define SW_RESET_CTL_N_BIT 5

/* PCIE_MPLLA_REG */
// divider enable
// Gen 1/2: 1
// Gen3 : 0
#define MPLLA_DIV5_CLK_EN_MASK 0x00000001U
#define MPLLA_DIV5_CLK_EN_BIT 0
// charge pump control
// Gen 1/2: 20
// Gen3 : 7
#define MPLLA_CP_INT_MASK 0x000000FEU
#define MPLLA_CP_INT_SHIFT 1U
// charge pump control
// Gen 1/2: 63
// Gen3 : 37
#define MPLLA_CP_PROP_MASK 0x00007F00U
#define MPLLA_CP_PROP_SHIFT 8U
// multiplier control
// Gen 1/2: 18
// Gen3 : 128
#define MPLLA_MULTIPLIER_MASK 0x0FFF0000U
#define MPLLA_MULTIPLIER_SHIFT 16U
// reference clock divider
// Gen 1/2: 0
// Gen3 : 1
#define REF_CLK_MPLLA_DIV_MASK 0x70000000U
#define REF_CLK_MPLLA_DIV_SHIFT 28U
// force mplla to be powered up
#define MPLLA_FORCE_EN_MASK 0x80000000U
#define MPLLA_FORCE_EN_BIT 31

/* PCIE_MPLLA_SSC1_REG */
// spread spectrum enable
#define MPLLA_SSC_UP_SPREAD_MASK 0x00000001U
#define MPLLA_SSC_UP_SPREAD_BIT 0
// spread spectrum peak
#define MPLLA_SSC_PEAK_MASK 0x003FFFFEU
#define MPLLA_SSC_PEAK_SHIFT 1U

/* PCIE_MPLLA_SSC2_REG */
// spread spectrum stepsize
#define MPLLA_SSC_STEPSIZE_MASK 0x003FFFFFU
#define MPLLA_SSC_STEPSIZE_SHIFT 0U

/* PCIE_MPLLB_REG */
// divider enable
// Gen 1/2: 1
// Gen3 : 0
#define MPLLB_DIV5_CLK_EN_MASK 0x00000001U
#define MPLLB_DIV5_CLK_EN_BIT 0
// charge pump control
// Gen 1/2: 20
// Gen3 : 7
#define MPLLB_CP_INT_MASK 0x000000FEU
#define MPLLB_CP_INT_SHIFT 1U
// charge pump control
// Gen 1/2: 63
// Gen3 : 37
#define MPLLB_CP_PROP_MASK 0x00007F00U
#define MPLLB_CP_PROP_SHIFT 8U
// multiplier control
// Gen 1/2: 18
// Gen3 : 128
#define MPLLB_MULTIPLIER_MASK 0x0FFF0000U
#define MPLLB_MULTIPLIER_SHIFT 16U
// reference clock divider
// Gen 1/2: 0
// Gen3 : 1
#define REF_CLK_MPLLB_DIV_MASK 0x70000000U
#define REF_CLK_MPLLB_DIV_SHIFT 28U
// force mpllb to be powered up
#define MPLLB_FORCE_EN_MASK 0x80000000U
#define MPLLB_FORCE_EN_BIT 31

/* PCIE_MPLLB_SSC1_REG */
// spread spectrum enable
#define MPLLB_SSC_UP_SPREAD_MASK 0x00000001U
#define MPLLB_SSC_UP_SPREAD_BIT 0
// spread spectrum peak
#define MPLLB_SSC_PEAK_MASK 0x003FFFFEU
#define MPLLB_SSC_PEAK_SHIFT 1U

/* PCIE_MPLLB_SSC2_REG */
// spread spectrum stepsize
#define MPLLB_SSC_STEPSIZE_MASK 0x003FFFFFU
#define MPLLB_SSC_STEPSIZE_SHIFT 0U

/* PCIE_TX_EQ_G1_REG */
// equalization parameter
#define TX_EQ_MAIN_G1_MASK 0x0000003FU
#define TX_EQ_MAIN_G1_SHIFT 0U
// equalization parameter
#define TX_EQ_OVRD_G1_MASK 0x00000100U
#define TX_EQ_OVRD_G1_BIT 8
// equalization parameter
#define TX_EQ_POST_G1_MASK 0x003F0000U
#define TX_EQ_POST_G1_SHIFT 16U
// equalization parameter
#define TX_EQ_PRE_G1_MASK 0x3F000000U
#define TX_EQ_PRE_G1_SHIFT 24U

/* PCIE_TX_EQ_G2_REG */
// equalization parameter
#define TX_EQ_MAIN_G2_MASK 0x0000003FU
#define TX_EQ_MAIN_G2_SHIFT 0U
// equalization parameter
#define TX_EQ_OVRD_G2_MASK 0x00000100U
#define TX_EQ_OVRD_G2_BIT 8
// equalization parameter
#define TX_EQ_POST_G2_MASK 0x003F0000U
#define TX_EQ_POST_G2_SHIFT 16U
// equalization parameter
#define TX_EQ_PRE_G2_MASK 0x3F000000U
#define TX_EQ_PRE_G2_SHIFT 24U

/* PCIE_TX_EQ_G3_REG */
// equalization parameter
#define TX_EQ_MAIN_G3_MASK 0x0000003FU
#define TX_EQ_MAIN_G3_SHIFT 0U
// equalization parameter
#define TX_EQ_OVRD_G3_MASK 0x00000100U
#define TX_EQ_OVRD_G3_BIT 8
// equalization parameter
#define TX_EQ_POST_G3_MASK 0x003F0000U
#define TX_EQ_POST_G3_SHIFT 16U
// equalization parameter
#define TX_EQ_PRE_G3_MASK 0x3F000000U
#define TX_EQ_PRE_G3_SHIFT 24U

/* PCIE_FW_RAM_CTRL_REG */
// PHY 0 FW SRAM bypass enable
#define PHY0_SRAM_BYPASS_MASK 0x00000001U
#define PHY0_SRAM_BYPASS_BIT 0
// PHY 1 FW SRAM bypass enable
#define PHY1_SRAM_BYPASS_MASK 0x00000002U
#define PHY1_SRAM_BYPASS_BIT 1
// PHY 2 FW SRAM bypass enable
#define PHY2_SRAM_BYPASS_MASK 0x00000004U
#define PHY2_SRAM_BYPASS_BIT 2
// PHY 3 FW SRAM bypass enable
#define PHY3_SRAM_BYPASS_MASK 0x00000008U
#define PHY3_SRAM_BYPASS_BIT 3
// PHY 0 FW SRAM external load done
#define PHY0_SRAM_EXT_LD_DONE_MASK 0x00000100U
#define PHY0_SRAM_EXT_LD_DONE_BIT 8
// PHY 1 FW SRAM external load done
#define PHY1_SRAM_EXT_LD_DONE_MASK 0x00000200U
#define PHY1_SRAM_EXT_LD_DONE_BIT 9
// PHY 2 FW SRAM external load done
#define PHY2_SRAM_EXT_LD_DONE_MASK 0x00000400U
#define PHY2_SRAM_EXT_LD_DONE_BIT 10
// PHY 3 FW SRAM external load done
#define PHY3_SRAM_EXT_LD_DONE_MASK 0x00000800U
#define PHY3_SRAM_EXT_LD_DONE_BIT 11

/* PCIE_FW_RAM_STAT_REG */
// PHY 0 FW RAM init done
#define PHY0_SRAM_INIT_DONE_MASK 0x00000001U
#define PHY0_SRAM_INIT_DONE_BIT 0
// PHY 1 FW RAM init done
#define PHY1_SRAM_INIT_DONE_MASK 0x00000002U
#define PHY1_SRAM_INIT_DONE_BIT 1
// PHY 2 FW RAM init done
#define PHY2_SRAM_INIT_DONE_MASK 0x00000004U
#define PHY2_SRAM_INIT_DONE_BIT 2
// PHY 3 FW RAM init done
#define PHY3_SRAM_INIT_DONE_MASK 0x00000008U
#define PHY3_SRAM_INIT_DONE_BIT 3

/* PCIE_PHY_EXTENDED_CFG_REG */
// PHY 0 extended configuration
#define PHY_TX0_VREGDRV_BYP_MASK 0x00000001U
#define PHY_TX0_VREGDRV_BYP_BIT 0
// PHY 0 extended configuration
#define PHY_RX0_DIV16P5_CLK_EN_MASK 0x00000002U
#define PHY_RX0_DIV16P5_CLK_EN_BIT 1
// PHY 0 extended configuration
#define PHY_RX0_125MHZ_CLK_EN_MASK 0x00000004U
#define PHY_RX0_125MHZ_CLK_EN_BIT 2
// PHY 0 extended configuration
#define PHY_RX0_TERM_ACDC_MASK 0x00000008U
#define PHY_RX0_TERM_ACDC_BIT 3
// PHY 1 extended configuration
#define PHY_TX1_VREGDRV_BYP_MASK 0x00000100U
#define PHY_TX1_VREGDRV_BYP_BIT 8
// PHY 1 extended configuration
#define PHY_RX1_DIV16P5_CLK_EN_MASK 0x00000200U
#define PHY_RX1_DIV16P5_CLK_EN_BIT 9
// PHY 1 extended configuration
#define PHY_RX1_125MHZ_CLK_EN_MASK 0x00000400U
#define PHY_RX1_125MHZ_CLK_EN_BIT 10
// PHY 1 extended configuration
#define PHY_RX1_TERM_ACDC_MASK 0x00000800U
#define PHY_RX1_TERM_ACDC_BIT 11
// PHY 2 extended configuration
#define PHY_TX2_VREGDRV_BYP_MASK 0x00010000U
#define PHY_TX2_VREGDRV_BYP_BIT 16
// PHY 2 extended configuration
#define PHY_RX2_DIV16P5_CLK_EN_MASK 0x00020000U
#define PHY_RX2_DIV16P5_CLK_EN_BIT 17
// PHY 2 extended configuration
#define PHY_RX2_125MHZ_CLK_EN_MASK 0x00040000U
#define PHY_RX2_125MHZ_CLK_EN_BIT 18
// PHY 2 extended configuration
#define PHY_RX2_TERM_ACDC_MASK 0x00080000U
#define PHY_RX2_TERM_ACDC_BIT 19
// PHY 3 extended configuration
#define PHY_TX3_VREGDRV_BYP_MASK 0x01000000U
#define PHY_TX3_VREGDRV_BYP_BIT 24
// PHY 3 extended configuration
#define PHY_RX3_DIV16P5_CLK_EN_MASK 0x02000000U
#define PHY_RX3_DIV16P5_CLK_EN_BIT 25
// PHY 3 extended configuration
#define PHY_RX3_125MHZ_CLK_EN_MASK 0x04000000U
#define PHY_RX3_125MHZ_CLK_EN_BIT 26
// PHY 3 extended configuration
#define PHY_RX3_TERM_ACDC_MASK 0x08000000U
#define PHY_RX3_TERM_ACDC_BIT 27

/* PCIE_PHY_CR_CTRL_0_REG - PCIE_PHY_CR_CTRL_3_REG */
// CR interface clock
#define PHY_CR_PARA_CLK_MASK 0x00000001U
#define PHY_CR_PARA_CLK_BIT 0
// CR interface read enable
#define PHY_CR_PARA_RD_EN_MASK 0x00000002U
#define PHY_CR_PARA_RD_EN_BIT 1
// CR interface parallel interface select, 0: JTAG 1: CR
#define PHY_CR_PARA_SEL_MASK 0x00000004U
#define PHY_CR_PARA_SEL_BIT 2
// CR interface write enable
#define PHY_CR_PARA_WR_EN_MASK 0x00000008U
#define PHY_CR_PARA_WR_EN_BIT 3

/* PCIE_PHY_CR_WR_DA_0_REG - PCIE_PHY_CR_WR_DA_3_REG */
// CR interface address
#define PHY_CR_PARA_ADDR_MASK 0x0000FFFFU
#define PHY_CR_PARA_ADDR_SHIFT 0U
// CR interface write data
#define PHY_CR_PARA_WR_DATA_MASK 0xFFFF0000U
#define PHY_CR_PARA_WR_DATA_SHIFT 16U

/* PCIE_PHY_CR_RD_0_REG - PCIE_PHY_CR_RD_3_REG */
// CR interface read data
#define PHY_CR_PARA_RD_DATA_MASK 0x0000FFFFU
#define PHY_CR_PARA_RD_DATA_SHIFT 0U
// CR interface read acknowledge
#define PHY_CR_PARA_ACK_MASK 0x00010000U
#define PHY_CR_PARA_ACK_BIT 16

/* PCIE_ISM_REG */
//           Wake Up. Wake up from power management unit.
// The controller generates wake to request the system to restore power and clock when a wakeup event has been detected.
#define WAKE_ISM_MASK 0x00000001U
#define WAKE_ISM_BIT 0
//           Indicates that any bit in the Slot Status register transitions from 0 to 1
// and the associated event notification is enabled in the Slot Control register.
// The PME Enable bit in the Power Management Control and Status register must be set to 1.
#define HP_PME_ISM_MASK 0x00000002U
#define HP_PME_ISM_BIT 1
//           Indicates that the PME Status bit in the Root Status register is set to 1
// and the INTx Assertion is enabled
// and the PME Interrupt is enabled.
#define CFG_PME_INT_ISM_MASK 0x00000004U
#define CFG_PME_INT_ISM_BIT 2
//           Indicates that the Link Equalization Request bit in the Link Status 2 Register has been set
// and the Link Equalization Request Interrupt Enable (Link Control 3 Register bit 1) is set.
#define CFG_LINK_EQ_REQ_INT_ISM_MASK 0x00000008U
#define CFG_LINK_EQ_REQ_INT_ISM_BIT 3
//           Indicates that the Link Autonomous Bandwidth Interrupt Status bit in the Link Status register is set to 1
// and the INTx Assertion is enabled
// and Link Autonomous Bandwidth Interrupt is enabled.
#define CFG_LINK_AUTO_BW_INT_ISM_MASK 0x00000010U
#define CFG_LINK_AUTO_BW_INT_ISM_BIT 4
//           Indicates that the Bandwidth Management Interrupt Status bit in the Link Status register is set to 1
// and the INTx Assertion is enabled
// and Bandwidth Management Interrupt is enabled.
#define CFG_BW_MGT_INT_ISM_MASK 0x00000020U
#define CFG_BW_MGT_INT_ISM_BIT 5
// Indicates that a MSI is received.
#define MSI_CTRL_INT_ISM_MASK 0x00000040U
#define MSI_CTRL_INT_ISM_BIT 6

/* PCIE_RIS_REG */
// raw interrupt status
#define WAKE_RIS_MASK 0x00000001U
#define WAKE_RIS_BIT 0
// raw interrupt status
#define HP_PME_RIS_MASK 0x00000002U
#define HP_PME_RIS_BIT 1
// raw interrupt status
#define CFG_PME_INT_RIS_MASK 0x00000004U
#define CFG_PME_INT_RIS_BIT 2
// raw interrupt status
#define CFG_LINK_EQ_REQ_INT_RIS_MASK 0x00000008U
#define CFG_LINK_EQ_REQ_INT_RIS_BIT 3
// raw interrupt status
#define CFG_LINK_AUTO_BW_INT_RIS_MASK 0x00000010U
#define CFG_LINK_AUTO_BW_INT_RIS_BIT 4
// raw interrupt status
#define CFG_BW_MGT_INT_RIS_MASK 0x00000020U
#define CFG_BW_MGT_INT_RIS_BIT 5
// raw interrupt status
#define MSI_CTRL_INT_RIS_MASK 0x00000040U
#define MSI_CTRL_INT_RIS_BIT 6

/* PCIE_MIS_REG */
// masked interrupt status
#define WAKE_MIS_MASK 0x00000001U
#define WAKE_MIS_BIT 0
// masked interrupt status
#define HP_PME_MIS_MASK 0x00000002U
#define HP_PME_MIS_BIT 1
// masked interrupt status
#define CFG_PME_INT_MIS_MASK 0x00000004U
#define CFG_PME_INT_MIS_BIT 2
// masked interrupt status
#define CFG_LINK_EQ_REQ_INT_MIS_MASK 0x00000008U
#define CFG_LINK_EQ_REQ_INT_MIS_BIT 3
// masked interrupt status
#define CFG_LINK_AUTO_BW_INT_MIS_MASK 0x00000010U
#define CFG_LINK_AUTO_BW_INT_MIS_BIT 4
// masked interrupt status
#define CFG_BW_MGT_INT_MIS_MASK 0x00000020U
#define CFG_BW_MGT_INT_MIS_BIT 5
// masked interrupt status
#define MSI_CTRL_INT_MIS_MASK 0x00000040U
#define MSI_CTRL_INT_MIS_BIT 6

/* PCIE_ISC_REG */
// interrupt status clear
#define WAKE_ISC_MASK 0x00000001U
#define WAKE_ISC_BIT 0
// interrupt status clear
#define HP_PME_ISC_MASK 0x00000002U
#define HP_PME_ISC_BIT 1
// interrupt status clear
#define CFG_PME_INT_ISC_MASK 0x00000004U
#define CFG_PME_INT_ISC_BIT 2
// interrupt status clear
#define CFG_LINK_EQ_REQ_INT_ISC_MASK 0x00000008U
#define CFG_LINK_EQ_REQ_INT_ISC_BIT 3
// interrupt status clear
#define CFG_LINK_AUTO_BW_INT_ISC_MASK 0x00000010U
#define CFG_LINK_AUTO_BW_INT_ISC_BIT 4
// interrupt status clear
#define CFG_BW_MGT_INT_ISC_MASK 0x00000020U
#define CFG_BW_MGT_INT_ISC_BIT 5
// interrupt status clear
#define MSI_CTRL_INT_ISC_MASK 0x00000040U
#define MSI_CTRL_INT_ISC_BIT 6

/* PCIE_ISS_REG */
// interrupt status set
#define WAKE_ISS_MASK 0x00000001U
#define WAKE_ISS_BIT 0
// interrupt status set
#define HP_PME_ISS_MASK 0x00000002U
#define HP_PME_ISS_BIT 1
// interrupt status set
#define CFG_PME_INT_ISS_MASK 0x00000004U
#define CFG_PME_INT_ISS_BIT 2
// interrupt status set
#define CFG_LINK_EQ_REQ_INT_ISS_MASK 0x00000008U
#define CFG_LINK_EQ_REQ_INT_ISS_BIT 3
// interrupt status set
#define CFG_LINK_AUTO_BW_INT_ISS_MASK 0x00000010U
#define CFG_LINK_AUTO_BW_INT_ISS_BIT 4
// interrupt status set
#define CFG_BW_MGT_INT_ISS_MASK 0x00000020U
#define CFG_BW_MGT_INT_ISS_BIT 5
// interrupt status set
#define MSI_CTRL_INT_ISS_MASK 0x00000040U
#define MSI_CTRL_INT_ISS_BIT 6

/* PCIE_ERROR_ISM_REG */
// ATU Error indication per ATU region.
#define RADM_TRGT1_ATU_CBUF_ERR_ISM_MASK 0x0000000FU
#define RADM_TRGT1_ATU_CBUF_ERR_ISM_SHIFT 0U
// Indicates that the controller received an ERR_NONFATAL message.
#define RADM_NONFATAL_ERR_ISM_MASK 0x00000010U
#define RADM_NONFATAL_ERR_ISM_BIT 4
// Indicates that the controller received an ERR_FATAL message.
#define RADM_FATAL_ERR_ISM_MASK 0x00000020U
#define RADM_FATAL_ERR_ISM_BIT 5
// Indicates that the completion TLP for a request has not been received within the expected time window.
#define RADM_CPL_TIMEOUT_ISM_MASK 0x00000040U
#define RADM_CPL_TIMEOUT_ISM_BIT 6
// Indicates that the controller received an ERR_COR message.
#define RADM_CORRECTABLE_ERR_ISM_MASK 0x00000080U
#define RADM_CORRECTABLE_ERR_ISM_BIT 7
// Interface timeout status
#define IF_TIMEOUT_STATUS_ISM_MASK 0x00000100U
#define IF_TIMEOUT_STATUS_ISM_BIT 8
// Indicates that the controller has detected an Uncorrectable Internal Error.
#define CFG_UNCOR_INTERNAL_ERR_STS_ISM_MASK 0x00000200U
#define CFG_UNCOR_INTERNAL_ERR_STS_ISM_BIT 9
// Indicates that the controller has detected a Replay Timer Timeout.
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_ISM_MASK 0x00000400U
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_ISM_BIT 10
// Indicates that the controller has detected a REPLAY_NUMBER Rollover Error.
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_ISM_MASK 0x00000800U
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_ISM_BIT 11
// Indicates that the controller has detected a Receiver Overflow Error.
#define CFG_RCVR_OVERFLOW_ERR_STS_ISM_MASK 0x00001000U
#define CFG_RCVR_OVERFLOW_ERR_STS_ISM_BIT 12
// Indicates that the controller has detected a Receiver Error.
#define CFG_RCVR_ERR_STS_ISM_MASK 0x00002000U
#define CFG_RCVR_ERR_STS_ISM_BIT 13
// Indicates that the controller has detected a Malformed TLP Error.
#define CFG_MLF_TLP_ERR_STS_ISM_MASK 0x00004000U
#define CFG_MLF_TLP_ERR_STS_ISM_BIT 14
// Indicates that the controller has detected a Flow Control Protocol Error.
#define CFG_FC_PROTOCOL_ERR_STS_ISM_MASK 0x00008000U
#define CFG_FC_PROTOCOL_ERR_STS_ISM_BIT 15
// Indicates that the controller has detected an ECRC Error.
#define CFG_ECRC_ERR_STS_ISM_MASK 0x00010000U
#define CFG_ECRC_ERR_STS_ISM_BIT 16
// Indicates that the controller has detected a Data Link Protocol Error.
#define CFG_DL_PROTOCOL_ERR_STS_ISM_MASK 0x00020000U
#define CFG_DL_PROTOCOL_ERR_STS_ISM_BIT 17
// Indicates that the controller has detected an Corrected Internal Error.
#define CFG_CORRECTED_INTERNAL_ERR_STS_ISM_MASK 0x00040000U
#define CFG_CORRECTED_INTERNAL_ERR_STS_ISM_BIT 18
// Indicates that the controller has detected a Bad TLP Error.
#define CFG_BAD_TLP_ERR_STS_ISM_MASK 0x00080000U
#define CFG_BAD_TLP_ERR_STS_ISM_BIT 19
// Indicates that the controller has detected a Bad DLLP Error.
#define CFG_BAD_DLLP_ERR_STS_ISM_MASK 0x00100000U
#define CFG_BAD_DLLP_ERR_STS_ISM_BIT 20
// Indicates that a reported error condition causes a bit to be set in the Root Error Status register.
#define CFG_AER_RC_ERR_INT_ISM_MASK 0x00200000U
#define CFG_AER_RC_ERR_INT_ISM_BIT 21
// Indication from the controller that it has entered RASDP error mode.
#define AXI_PCIE_TX_RASDP_ERR_MODE_ISM_MASK 0x00400000U
#define AXI_PCIE_TX_RASDP_ERR_MODE_ISM_BIT 22
// Indication from the controller that it has entered RASDP error mode.
#define AXI_PCIE_RX_RASDP_ERR_MODE_ISM_MASK 0x00800000U
#define AXI_PCIE_RX_RASDP_ERR_MODE_ISM_BIT 23
//           Indicates that the controller detected a datapath parity error, one bit for each of the following parity errors:
// [0] Parity error at front end of the transmit datapath,
// [1] Parity error at back end of the transmit datapath,
// [2] Parity error the receive datapath.
#define APP_PARITY_ERRS_ISM_MASK 0x07000000U
#define APP_PARITY_ERRS_ISM_SHIFT 24U

/* PCIE_ERROR_RIS_REG */
// raw interrupt status
#define RADM_TRGT1_ATU_CBUF_ERR_RIS_MASK 0x0000000FU
#define RADM_TRGT1_ATU_CBUF_ERR_RIS_SHIFT 0U
// raw interrupt status
#define RADM_NONFATAL_ERR_RIS_MASK 0x00000010U
#define RADM_NONFATAL_ERR_RIS_BIT 4
// raw interrupt status
#define RADM_FATAL_ERR_RIS_MASK 0x00000020U
#define RADM_FATAL_ERR_RIS_BIT 5
// raw interrupt status
#define RADM_CPL_TIMEOUT_RIS_MASK 0x00000040U
#define RADM_CPL_TIMEOUT_RIS_BIT 6
// raw interrupt status
#define RADM_CORRECTABLE_ERR_RIS_MASK 0x00000080U
#define RADM_CORRECTABLE_ERR_RIS_BIT 7
// raw interrupt status
#define IF_TIMEOUT_STATUS_RIS_MASK 0x00000100U
#define IF_TIMEOUT_STATUS_RIS_BIT 8
// raw interrupt status
#define CFG_UNCOR_INTERNAL_ERR_STS_RIS_MASK 0x00000200U
#define CFG_UNCOR_INTERNAL_ERR_STS_RIS_BIT 9
// raw interrupt status
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_RIS_MASK 0x00000400U
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_RIS_BIT 10
// raw interrupt status
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_RIS_MASK 0x00000800U
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_RIS_BIT 11
// raw interrupt status
#define CFG_RCVR_OVERFLOW_ERR_STS_RIS_MASK 0x00001000U
#define CFG_RCVR_OVERFLOW_ERR_STS_RIS_BIT 12
// raw interrupt status
#define CFG_RCVR_ERR_STS_RIS_MASK 0x00002000U
#define CFG_RCVR_ERR_STS_RIS_BIT 13
// raw interrupt status
#define CFG_MLF_TLP_ERR_STS_RIS_MASK 0x00004000U
#define CFG_MLF_TLP_ERR_STS_RIS_BIT 14
// raw interrupt status
#define CFG_FC_PROTOCOL_ERR_STS_RIS_MASK 0x00008000U
#define CFG_FC_PROTOCOL_ERR_STS_RIS_BIT 15
// raw interrupt status
#define CFG_ECRC_ERR_STS_RIS_MASK 0x00010000U
#define CFG_ECRC_ERR_STS_RIS_BIT 16
// raw interrupt status
#define CFG_DL_PROTOCOL_ERR_STS_RIS_MASK 0x00020000U
#define CFG_DL_PROTOCOL_ERR_STS_RIS_BIT 17
// raw interrupt status
#define CFG_CORRECTED_INTERNAL_ERR_STS_RIS_MASK 0x00040000U
#define CFG_CORRECTED_INTERNAL_ERR_STS_RIS_BIT 18
// raw interrupt status
#define CFG_BAD_TLP_ERR_STS_RIS_MASK 0x00080000U
#define CFG_BAD_TLP_ERR_STS_RIS_BIT 19
// raw interrupt status
#define CFG_BAD_DLLP_ERR_STS_RIS_MASK 0x00100000U
#define CFG_BAD_DLLP_ERR_STS_RIS_BIT 20
// raw interrupt status
#define CFG_AER_RC_ERR_INT_RIS_MASK 0x00200000U
#define CFG_AER_RC_ERR_INT_RIS_BIT 21
// raw interrupt status
#define AXI_PCIE_TX_RASDP_ERR_MODE_RIS_MASK 0x00400000U
#define AXI_PCIE_TX_RASDP_ERR_MODE_RIS_BIT 22
// raw interrupt status
#define AXI_PCIE_RX_RASDP_ERR_MODE_RIS_MASK 0x00800000U
#define AXI_PCIE_RX_RASDP_ERR_MODE_RIS_BIT 23
// raw interrupt status
#define APP_PARITY_ERRS_RIS_MASK 0x07000000U
#define APP_PARITY_ERRS_RIS_SHIFT 24U

/* PCIE_ERROR_MIS_REG */
// masked interrupt status
#define RADM_TRGT1_ATU_CBUF_ERR_MIS_MASK 0x0000000FU
#define RADM_TRGT1_ATU_CBUF_ERR_MIS_SHIFT 0U
// masked interrupt status
#define RADM_NONFATAL_ERR_MIS_MASK 0x00000010U
#define RADM_NONFATAL_ERR_MIS_BIT 4
// masked interrupt status
#define RADM_FATAL_ERR_MIS_MASK 0x00000020U
#define RADM_FATAL_ERR_MIS_BIT 5
// masked interrupt status
#define RADM_CPL_TIMEOUT_MIS_MASK 0x00000040U
#define RADM_CPL_TIMEOUT_MIS_BIT 6
// masked interrupt status
#define RADM_CORRECTABLE_ERR_MIS_MASK 0x00000080U
#define RADM_CORRECTABLE_ERR_MIS_BIT 7
// masked interrupt status
#define IF_TIMEOUT_STATUS_MIS_MASK 0x00000100U
#define IF_TIMEOUT_STATUS_MIS_BIT 8
// masked interrupt status
#define CFG_UNCOR_INTERNAL_ERR_STS_MIS_MASK 0x00000200U
#define CFG_UNCOR_INTERNAL_ERR_STS_MIS_BIT 9
// masked interrupt status
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_MIS_MASK 0x00000400U
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_MIS_BIT 10
// masked interrupt status
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_MIS_MASK 0x00000800U
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_MIS_BIT 11
// masked interrupt status
#define CFG_RCVR_OVERFLOW_ERR_STS_MIS_MASK 0x00001000U
#define CFG_RCVR_OVERFLOW_ERR_STS_MIS_BIT 12
// masked interrupt status
#define CFG_RCVR_ERR_STS_MIS_MASK 0x00002000U
#define CFG_RCVR_ERR_STS_MIS_BIT 13
// masked interrupt status
#define CFG_MLF_TLP_ERR_STS_MIS_MASK 0x00004000U
#define CFG_MLF_TLP_ERR_STS_MIS_BIT 14
// masked interrupt status
#define CFG_FC_PROTOCOL_ERR_STS_MIS_MASK 0x00008000U
#define CFG_FC_PROTOCOL_ERR_STS_MIS_BIT 15
// masked interrupt status
#define CFG_ECRC_ERR_STS_MIS_MASK 0x00010000U
#define CFG_ECRC_ERR_STS_MIS_BIT 16
// masked interrupt status
#define CFG_DL_PROTOCOL_ERR_STS_MIS_MASK 0x00020000U
#define CFG_DL_PROTOCOL_ERR_STS_MIS_BIT 17
// masked interrupt status
#define CFG_CORRECTED_INTERNAL_ERR_STS_MIS_MASK 0x00040000U
#define CFG_CORRECTED_INTERNAL_ERR_STS_MIS_BIT 18
// masked interrupt status
#define CFG_BAD_TLP_ERR_STS_MIS_MASK 0x00080000U
#define CFG_BAD_TLP_ERR_STS_MIS_BIT 19
// masked interrupt status
#define CFG_BAD_DLLP_ERR_STS_MIS_MASK 0x00100000U
#define CFG_BAD_DLLP_ERR_STS_MIS_BIT 20
// masked interrupt status
#define CFG_AER_RC_ERR_INT_MIS_MASK 0x00200000U
#define CFG_AER_RC_ERR_INT_MIS_BIT 21
// masked interrupt status
#define AXI_PCIE_TX_RASDP_ERR_MODE_MIS_MASK 0x00400000U
#define AXI_PCIE_TX_RASDP_ERR_MODE_MIS_BIT 22
// masked interrupt status
#define AXI_PCIE_RX_RASDP_ERR_MODE_MIS_MASK 0x00800000U
#define AXI_PCIE_RX_RASDP_ERR_MODE_MIS_BIT 23
// masked interrupt status
#define APP_PARITY_ERRS_MIS_MASK 0x07000000U
#define APP_PARITY_ERRS_MIS_SHIFT 24U

/* PCIE_ERROR_ISC_REG */
// interrupt status clear
#define RADM_TRGT1_ATU_CBUF_ERR_ISC_MASK 0x0000000FU
#define RADM_TRGT1_ATU_CBUF_ERR_ISC_SHIFT 0U
// interrupt status clear
#define RADM_NONFATAL_ERR_ISC_MASK 0x00000010U
#define RADM_NONFATAL_ERR_ISC_BIT 4
// interrupt status clear
#define RADM_FATAL_ERR_ISC_MASK 0x00000020U
#define RADM_FATAL_ERR_ISC_BIT 5
// interrupt status clear
#define RADM_CPL_TIMEOUT_ISC_MASK 0x00000040U
#define RADM_CPL_TIMEOUT_ISC_BIT 6
// interrupt status clear
#define RADM_CORRECTABLE_ERR_ISC_MASK 0x00000080U
#define RADM_CORRECTABLE_ERR_ISC_BIT 7
// interrupt status clear
#define IF_TIMEOUT_STATUS_ISC_MASK 0x00000100U
#define IF_TIMEOUT_STATUS_ISC_BIT 8
// interrupt status clear
#define CFG_UNCOR_INTERNAL_ERR_STS_ISC_MASK 0x00000200U
#define CFG_UNCOR_INTERNAL_ERR_STS_ISC_BIT 9
// interrupt status clear
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_ISC_MASK 0x00000400U
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_ISC_BIT 10
// interrupt status clear
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_ISC_MASK 0x00000800U
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_ISC_BIT 11
// interrupt status clear
#define CFG_RCVR_OVERFLOW_ERR_STS_ISC_MASK 0x00001000U
#define CFG_RCVR_OVERFLOW_ERR_STS_ISC_BIT 12
// interrupt status clear
#define CFG_RCVR_ERR_STS_ISC_MASK 0x00002000U
#define CFG_RCVR_ERR_STS_ISC_BIT 13
// interrupt status clear
#define CFG_MLF_TLP_ERR_STS_ISC_MASK 0x00004000U
#define CFG_MLF_TLP_ERR_STS_ISC_BIT 14
// interrupt status clear
#define CFG_FC_PROTOCOL_ERR_STS_ISC_MASK 0x00008000U
#define CFG_FC_PROTOCOL_ERR_STS_ISC_BIT 15
// interrupt status clear
#define CFG_ECRC_ERR_STS_ISC_MASK 0x00010000U
#define CFG_ECRC_ERR_STS_ISC_BIT 16
// interrupt status clear
#define CFG_DL_PROTOCOL_ERR_STS_ISC_MASK 0x00020000U
#define CFG_DL_PROTOCOL_ERR_STS_ISC_BIT 17
// interrupt status clear
#define CFG_CORRECTED_INTERNAL_ERR_STS_ISC_MASK 0x00040000U
#define CFG_CORRECTED_INTERNAL_ERR_STS_ISC_BIT 18
// interrupt status clear
#define CFG_BAD_TLP_ERR_STS_ISC_MASK 0x00080000U
#define CFG_BAD_TLP_ERR_STS_ISC_BIT 19
// interrupt status clear
#define CFG_BAD_DLLP_ERR_STS_ISC_MASK 0x00100000U
#define CFG_BAD_DLLP_ERR_STS_ISC_BIT 20
// interrupt status clear
#define CFG_AER_RC_ERR_INT_ISC_MASK 0x00200000U
#define CFG_AER_RC_ERR_INT_ISC_BIT 21
// interrupt status clear
#define AXI_PCIE_TX_RASDP_ERR_MODE_ISC_MASK 0x00400000U
#define AXI_PCIE_TX_RASDP_ERR_MODE_ISC_BIT 22
// interrupt status clear
#define AXI_PCIE_RX_RASDP_ERR_MODE_ISC_MASK 0x00800000U
#define AXI_PCIE_RX_RASDP_ERR_MODE_ISC_BIT 23
// interrupt status clear
#define APP_PARITY_ERRS_ISC_MASK 0x07000000U
#define APP_PARITY_ERRS_ISC_SHIFT 24U

/* PCIE_ERROR_ISS_REG */
// interrupt status set
#define RADM_TRGT1_ATU_CBUF_ERR_ISS_MASK 0x0000000FU
#define RADM_TRGT1_ATU_CBUF_ERR_ISS_SHIFT 0U
// interrupt status set
#define RADM_NONFATAL_ERR_ISS_MASK 0x00000010U
#define RADM_NONFATAL_ERR_ISS_BIT 4
// interrupt status set
#define RADM_FATAL_ERR_ISS_MASK 0x00000020U
#define RADM_FATAL_ERR_ISS_BIT 5
// interrupt status set
#define RADM_CPL_TIMEOUT_ISS_MASK 0x00000040U
#define RADM_CPL_TIMEOUT_ISS_BIT 6
// interrupt status set
#define RADM_CORRECTABLE_ERR_ISS_MASK 0x00000080U
#define RADM_CORRECTABLE_ERR_ISS_BIT 7
// interrupt status set
#define IF_TIMEOUT_STATUS_ISS_MASK 0x00000100U
#define IF_TIMEOUT_STATUS_ISS_BIT 8
// interrupt status set
#define CFG_UNCOR_INTERNAL_ERR_STS_ISS_MASK 0x00000200U
#define CFG_UNCOR_INTERNAL_ERR_STS_ISS_BIT 9
// interrupt status set
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_ISS_MASK 0x00000400U
#define CFG_REPLAY_TIMER_TIMEOUT_ERR_STS_ISS_BIT 10
// interrupt status set
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_ISS_MASK 0x00000800U
#define CFG_REPLAY_NUMBER_ROLLOVER_ERR_STS_ISS_BIT 11
// interrupt status set
#define CFG_RCVR_OVERFLOW_ERR_STS_ISS_MASK 0x00001000U
#define CFG_RCVR_OVERFLOW_ERR_STS_ISS_BIT 12
// interrupt status set
#define CFG_RCVR_ERR_STS_ISS_MASK 0x00002000U
#define CFG_RCVR_ERR_STS_ISS_BIT 13
// interrupt status set
#define CFG_MLF_TLP_ERR_STS_ISS_MASK 0x00004000U
#define CFG_MLF_TLP_ERR_STS_ISS_BIT 14
// interrupt status set
#define CFG_FC_PROTOCOL_ERR_STS_ISS_MASK 0x00008000U
#define CFG_FC_PROTOCOL_ERR_STS_ISS_BIT 15
// interrupt status set
#define CFG_ECRC_ERR_STS_ISS_MASK 0x00010000U
#define CFG_ECRC_ERR_STS_ISS_BIT 16
// interrupt status set
#define CFG_DL_PROTOCOL_ERR_STS_ISS_MASK 0x00020000U
#define CFG_DL_PROTOCOL_ERR_STS_ISS_BIT 17
// interrupt status set
#define CFG_CORRECTED_INTERNAL_ERR_STS_ISS_MASK 0x00040000U
#define CFG_CORRECTED_INTERNAL_ERR_STS_ISS_BIT 18
// interrupt status set
#define CFG_BAD_TLP_ERR_STS_ISS_MASK 0x00080000U
#define CFG_BAD_TLP_ERR_STS_ISS_BIT 19
// interrupt status set
#define CFG_BAD_DLLP_ERR_STS_ISS_MASK 0x00100000U
#define CFG_BAD_DLLP_ERR_STS_ISS_BIT 20
// interrupt status set
#define CFG_AER_RC_ERR_INT_ISS_MASK 0x00200000U
#define CFG_AER_RC_ERR_INT_ISS_BIT 21
// interrupt status set
#define AXI_PCIE_TX_RASDP_ERR_MODE_ISS_MASK 0x00400000U
#define AXI_PCIE_TX_RASDP_ERR_MODE_ISS_BIT 22
// interrupt status set
#define AXI_PCIE_RX_RASDP_ERR_MODE_ISS_MASK 0x00800000U
#define AXI_PCIE_RX_RASDP_ERR_MODE_ISS_BIT 23
// interrupt status set
#define APP_PARITY_ERRS_ISS_MASK 0x07000000U
#define APP_PARITY_ERRS_ISS_SHIFT 24U

#endif /* __PCIE_DCT__ */
