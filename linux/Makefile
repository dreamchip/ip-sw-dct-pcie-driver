#
#  Copyright (C) 2024 Dream Chip Technologies
#
#  SPDX-License-Identifier: GPL-2.0-only
#
#  Makefile for PCIe controller driver for DCT SoCs
#

SRC := $(shell pwd)

# add additional path to headers of modules we depend on
ccflags-y += -I$(M)/../common/regs
ccflags-y += -I$(M)/dwc
ccflags-y += $(EXTRA_INC)


ifeq ($(O),)
	include $(KERNEL_SRC)/.config
else
	include $(O)/.config
endif

obj-m := dct_pcie.o
dct_pcie-objs := dct_pcie_driver.o \
				 dwc/pcie-designware.o \
				 dwc/pcie-designware-ep.o \
				 dwc/pcie-designware-host.o \


all:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) O=$(O) modules

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) modules_install

clean:
	rm -f *.o *~ core .depend .*.cmd *.ko *.mod.c *.mod .*.d
	rm -f Module.markers Module.symvers modules.order modules.builtin
	rm -f */*.ko */*.mod.c */.*.mod.c */.*.cmd */*.o */.*.d */*.mod
	rm -f */modules.order */modules.builtin
	rm -rf .tmp_versions Modules.symvers

