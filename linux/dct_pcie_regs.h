/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 * dct_pcie_regs.h - PCIe controller Register for DCT SoCs
 * 
 */

#ifndef _DCT_PCIE_REGS_H_
#define _DCT_PCIE_REGS_H_


//
// registesr field helper macros
//
#define DCT_PCIE_REGS_FIELD_SET(reg, fieldname, val) (((reg) & ~(fieldname##_MASK)) | (((val) << (fieldname##_SHIFT)) & (fieldname##_MASK)))
#define DCT_PCIE_REGS_FIELD_GET(reg, fieldname)      (((reg) & (fieldname##_MASK)) >> (fieldname##_SHIFT))

#define DCT_PCIE_REGS_FLAG_SET(reg, flagname)        ((reg) | (flagname##_MASK))
#define DCT_PCIE_REGS_FLAG_CLEAR(reg, flagname)      ((reg) & ~(flagname##_MASK))
#define DCT_PCIE_REGS_FLAG_GET(reg, flagname)        (!!((reg) & (flagname##_MASK)))


//
// include mandatory sig register definitions
//
#include "pcie_dct_reg.h"
// define the PMI interface register to access the Phy Configuration Register Interface by bitbanging
#define PCIE_PMI_LANE_OFFSET   0x0c
#define PCIE_PMI_CTRL_OFF_REG(lane)  (PCIE_PHY_CR_CTRL_0_REG  + (lane) * PCIE_PMI_LANE_OFFSET)
#define PCIE_PMI_WR_DA_OFF_REG(lane) (PCIE_PHY_CR_WR_DA_0_REG + (lane) * PCIE_PMI_LANE_OFFSET)
#define PCIE_PMI_RD_OFF_REG(lane)    (PCIE_PHY_CR_RD_0_REG    + (lane) * PCIE_PMI_LANE_OFFSET)


//
// PCIe core irq bits
//
#define PCIE_IRQ_WAKE             0x00000001U
#define PCIE_IRQ_HP_PME           0x00000002U
#define PCIE_IRQ_CFG_PME          0x00000004U
#define PCIE_IRQ_CFG_LINK_EQ_REQ  0x00000008U
#define PCIE_IRQ_CFG_LINK_AUTO_BW 0x00000010U
#define PCIE_IRQ_CFG_BW_MGT       0x00000020U
#define PCIE_IRQ_MSI_CTRL         0x00000040U
// PCIe core error irq bits
#define PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_0          0x00000001U
#define PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_1          0x00000002U
#define PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_2          0x00000004U
#define PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_3          0x00000008U
#define PCIE_IRQERR_RADM_NONFATAL                  0x00000010U
#define PCIE_IRQERR_RADM_FATAL                     0x00000020U
#define PCIE_IRQERR_RADM_CPL_TIMEOUT               0x00000040U
#define PCIE_IRQERR_RADM_CORRECTABLE               0x00000080U
#define PCIE_IRQERR_IF_TIMEOUT_STATUS              0x00000100U
#define PCIE_IRQERR_CFG_UNCOR_INTERNAL_STS         0x00000200U
#define PCIE_IRQERR_CFG_REPLAY_TIMER_TIMEOUT_STS   0x00000400U
#define PCIE_IRQERR_CFG_REPLAY_NUMBER_ROLLOVER_STS 0x00000800U
#define PCIE_IRQERR_CFG_RCVR_OVERFLOW_STS          0x00001000U
#define PCIE_IRQERR_CFG_RCVR_STS                   0x00002000U
#define PCIE_IRQERR_CFG_MLF_TLP_STS                0x00004000U
#define PCIE_IRQERR_CFG_FC_PROTOCOL_STS            0x00008000U
#define PCIE_IRQERR_CFG_ECRC_STS                   0x00010000U
#define PCIE_IRQERR_CFG_DL_PROTOCOL_STS            0x00020000U
#define PCIE_IRQERR_CFG_CORRECTED_INTERNAL_STS     0x00040000U
#define PCIE_IRQERR_CFG_BAD_TLP_STS                0x00080000U
#define PCIE_IRQERR_CFG_BAD_DLLP_STS               0x00100000U
#define PCIE_IRQERR_CFG_AER_RC                     0x00200000U
#define PCIE_IRQERR_AXI_PCIE_TX_RASDP_MODE         0x00400000U
#define PCIE_IRQERR_AXI_PCIE_RX_RASDP_MODE         0x00800000U
#define PCIE_IRQERR_APP_PARITY                     0x07000000U
// PCIe core irq mask
#define PCIE_IRQ_MASK \
    ( PCIE_IRQ_WAKE \
    | PCIE_IRQ_HP_PME \
    | PCIE_IRQ_CFG_PME \
    | PCIE_IRQ_CFG_LINK_EQ_REQ \
    | PCIE_IRQ_CFG_LINK_AUTO_BW \
    | PCIE_IRQ_CFG_BW_MGT \
    | PCIE_IRQ_MSI_CTRL \
    )
// PCIe core error irq mask
#define PCIE_IRQERR_MASK \
    ( PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_0 \
    | PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_1 \
    | PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_2 \
    | PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_3 \
    | PCIE_IRQERR_RADM_NONFATAL \
    | PCIE_IRQERR_RADM_FATAL \
    | PCIE_IRQERR_RADM_CPL_TIMEOUT \
    | PCIE_IRQERR_RADM_CORRECTABLE \
    | PCIE_IRQERR_IF_TIMEOUT_STATUS \
    | PCIE_IRQERR_CFG_UNCOR_INTERNAL_STS \
    | PCIE_IRQERR_CFG_REPLAY_TIMER_TIMEOUT_STS \
    | PCIE_IRQERR_CFG_REPLAY_NUMBER_ROLLOVER_STS \
    | PCIE_IRQERR_CFG_RCVR_OVERFLOW_STS \
    | PCIE_IRQERR_CFG_RCVR_STS \
    | PCIE_IRQERR_CFG_MLF_TLP_STS \
    | PCIE_IRQERR_CFG_FC_PROTOCOL_STS \
    | PCIE_IRQERR_CFG_ECRC_STS \
    | PCIE_IRQERR_CFG_DL_PROTOCOL_STS \
    | PCIE_IRQERR_CFG_CORRECTED_INTERNAL_STS \
    | PCIE_IRQERR_CFG_BAD_TLP_STS \
    | PCIE_IRQERR_CFG_BAD_DLLP_STS \
    | PCIE_IRQERR_CFG_AER_RC \
    | PCIE_IRQERR_AXI_PCIE_TX_RASDP_MODE \
    | PCIE_IRQERR_AXI_PCIE_RX_RASDP_MODE \
    | PCIE_IRQERR_APP_PARITY \
    )


#define PL_DEBUG1_OFF__LINK_UP_MASK 0x00000010U
#define PL_DEBUG1_OFF__LINK_IN_TRAINING_MASK 0x20000000

#define LINK_CONTROL2_LINK_STATUS2_REG_REG 0x000000A0
#define LINK_CONTROL2_LINK_STATUS2_REG__PCIE_CAP_TX_MARGIN_MASK 0x00000380U
#define LINK_CONTROL2_LINK_STATUS2_REG__PCIE_CAP_TX_MARGIN_SHIFT 7U

#define PL_DEBUG1_OFF_REG 0x0000072C

#define MISC_CONTROL_1_OFF_REG 0x000008BC
#define MISC_CONTROL_1_OFF__DBI_RO_WR_EN_MASK 0x00000001U

#define GEN3_EQ_CONTROL_OFF_REG 0x000008A8
#define GEN3_EQ_CONTROL_OFF__GEN3_EQ_PSET_REQ_VEC_MASK 0x00FFFF00U
#define GEN3_EQ_CONTROL_OFF__GEN3_EQ_PSET_REQ_VEC_SHIFT 8U

#define LANE_SKEW_OFF_REG 0x00000714
#define LANE_SKEW_OFF__ELASTIC_BUFFER_MODE_MASK 0x04000000U

#define SPCIE_CAP_OFF_0CH_REG_REG 0x00000154

#define LANEX_DIG_ASIC_TX_ASIC_OUT_REG 0x00012028
#define LANEX_DIG_ASIC_TX_ASIC_OUT__TX_ACK_MASK 0x00000001U

#define LANEX_DIG_ASIC_RX_ASIC_OUT_0_REG 0x00012036
#define LANEX_DIG_ASIC_RX_ASIC_OUT_0__ACK_MASK 0x00000001U

#endif /* _DCT_PCIE_REGS_H_ */
