/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 * dct_pcie_driver.c - PCIe controller driver for DCT SoCs
 *
 * based on pcie-dra7xx.c taken from the Linux Kernel
 * Copyright (C) 2013-2014 Texas Instruments Incorporated - https://www.ti.com
 * Authors: Kishon Vijay Abraham I <kishon@ti.com>
 *
 */

#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/irqdomain.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/of_pci.h>
#include <linux/pci.h>
#include <linux/platform_device.h>
#include <linux/resource.h>
#include <linux/types.h>
#include <linux/mfd/syscon.h>
#include <linux/regmap.h>
#include <linux/gpio/consumer.h>

#include <../drivers/pci/pci.h>

#include "pcie-designware.h"

#include "dct_pcie_driver.h"


///////////////////////////////////////////////////////////////////////////////
// Prototypes
///////////////////////////////////////////////////////////////////////////////

static void dct_pcie_core_disable(struct dct_pcie *dct_pcie);
static int dct_pcie_core_enable(struct dct_pcie *dct_pcie);


///////////////////////////////////////////////////////////////////////////////
// LTSSM trace and debug functions
///////////////////////////////////////////////////////////////////////////////

#ifdef DCT_PCIE_TRACE_LTSSM
enum DCT_PCIE_LTSSM_STATE {
	S_DETECT_QUIET      = 0x00,
	S_DETECT_ACT        = 0x01,
	S_POLL_ACTIVE       = 0x02,
	S_POLL_COMPLIANCE   = 0x03,
	S_POLL_CONFIG       = 0x04,
	S_PRE_DETECT_QUIET  = 0x05,
	S_DETECT_WAIT       = 0x06,
	S_CFG_LINKWD_START  = 0x07,
	S_CFG_LINKWD_ACEPT  = 0x08,
	S_CFG_LANENUM_WAI   = 0x09,
	S_CFG_LANENUM_ACEPT = 0x0A,
	S_CFG_COMPLETE      = 0x0B,
	S_CFG_IDLE          = 0x0C,
	S_RCVRY_LOCK        = 0x0D,
	S_RCVRY_SPEED       = 0x0E,
	S_RCVRY_RCVRCFG     = 0x0F,
	S_RCVRY_IDLE        = 0x10,
	S_L0                = 0x11,
	S_L0S               = 0x12,
	S_L123_SEND_EIDLE   = 0x13,
	S_L1_IDLE           = 0x14,
	S_L2_IDLE           = 0x15,
	S_L2_WAKE           = 0x16,
	S_DISABLED_ENTRY    = 0x17,
	S_DISABLED_IDLE     = 0x18,
	S_DISABLED          = 0x19,
	S_LPBK_ENTRY        = 0x1A,
	S_LPBK_ACTIVE       = 0x1B,
	S_LPBK_EXIT         = 0x1C,
	S_LPBK_EXIT_TIMEOUT = 0x1D,
	S_HOT_RESET_ENTRY   = 0x1E,
	S_HOT_RESET         = 0x1F,
	S_RCVRY_EQ0         = 0x20,
	S_RCVRY_EQ1         = 0x21,
	S_RCVRY_EQ2         = 0x22,
	S_RCVRY_EQ3         = 0x23,
};

static const char *DCT_PCIE_LTSSM_STATE_NAME[] = {
	[0x00] = "DETECT_QUIET     ",
	[0x01] = "DETECT_ACT       ",
	[0x02] = "POLL_ACTIVE      ",
	[0x03] = "POLL_COMPLIANCE  ",
	[0x04] = "POLL_CONFIG      ",
	[0x05] = "PRE_DETECT_QUIET ",
	[0x06] = "DETECT_WAIT      ",
	[0x07] = "CFG_LINKWD_START ",
	[0x08] = "CFG_LINKWD_ACEPT ",
	[0x09] = "CFG_LANENUM_WAI  ",
	[0x0A] = "CFG_LANENUM_ACEPT",
	[0x0B] = "CFG_COMPLETE     ",
	[0x0C] = "CFG_IDLE         ",
	[0x0D] = "RCVRY_LOCK       ",
	[0x0E] = "RCVRY_SPEED      ",
	[0x0F] = "RCVRY_RCVRCFG    ",
	[0x10] = "RCVRY_IDLE       ",
	[0x11] = "L0               ",
	[0x12] = "L0S              ",
	[0x13] = "L123_SEND_EIDLE  ",
	[0x14] = "L1_IDLE          ",
	[0x15] = "L2_IDLE          ",
	[0x16] = "L2_WAKE          ",
	[0x17] = "DISABLED_ENTRY   ",
	[0x18] = "DISABLED_IDLE    ",
	[0x19] = "DISABLED         ",
	[0x1A] = "LPBK_ENTRY       ",
	[0x1B] = "LPBK_ACTIVE      ",
	[0x1C] = "LPBK_EXIT        ",
	[0x1D] = "LPBK_EXIT_TIMEOUT",
	[0x1E] = "HOT_RESET_ENTRY  ",
	[0x1F] = "HOT_RESET        ",
	[0x20] = "RCVRY_EQ0        ",
	[0x21] = "RCVRY_EQ1        ",
	[0x22] = "RCVRY_EQ2        ",
	[0x23] = "RCVRY_EQ3        ",
};

const char *dct_pcie_ltssm_state_name(enum DCT_PCIE_LTSSM_STATE state) {
    if ((state < S_DETECT_QUIET) || (state > S_RCVRY_EQ3)) {
        return "INVALID STATE    ";
    }
    return DCT_PCIE_LTSSM_STATE_NAME[state];
}

#ifdef DCT_PCIE_TRACE_LTSSM_INTERVAL_MS
static void dct_pcie_ltssm_monitor_func(struct work_struct *work)
{
	struct dct_pcie *dct_pcie = container_of(work, struct dct_pcie, ltssm_monitor.worker.work);

	if( gpiod_get_raw_value_cansleep(dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn].desc) ) {

		uint32_t status2 = dct_pcie_ctl_readl(dct_pcie, PCIE_STATUS2_REG);
		uint32_t status3 = dct_pcie_ctl_readl(dct_pcie, PCIE_STATUS3_REG);
		uint32_t debug1  = dct_pcie_dbi_readl(dct_pcie, PL_DEBUG1_OFF_REG) & (PL_DEBUG1_OFF__LINK_UP_MASK | PL_DEBUG1_OFF__LINK_IN_TRAINING_MASK);

		if ( (dct_pcie->ltssm_monitor.status2 != status2) ||
			(dct_pcie->ltssm_monitor.status3 != status3) ||
			(dct_pcie->ltssm_monitor.debug1 != debug1) ) {
				DCT_PCIE_TRACE_LTSSM_STATE_LOG(status3, status2, debug1);
				dct_pcie->ltssm_monitor.status2 = status2;
				dct_pcie->ltssm_monitor.status3 = status3;
				dct_pcie->ltssm_monitor.debug1 = debug1;
		}
	}

	if(dct_pcie->ltssm_monitor.running) {
		queue_delayed_work(dct_pcie->ltssm_monitor.workqueue, &dct_pcie->ltssm_monitor.worker, msecs_to_jiffies(DCT_PCIE_TRACE_LTSSM_INTERVAL_MS));
	}
}

static void dct_pcie_ltssm_monitor_start(struct dct_pcie *dct_pcie)
{
	dct_pcie->ltssm_monitor.running = true;
	DCT_PCIE_TRACE_LOG("LTSSM Monitor Worker Thread - Start");

	queue_delayed_work(dct_pcie->ltssm_monitor.workqueue, &dct_pcie->ltssm_monitor.worker, msecs_to_jiffies(0));
}

static void dct_pcie_ltssm_monitor_stop(struct dct_pcie *dct_pcie)
{
	dct_pcie->ltssm_monitor.running = false;
	DCT_PCIE_TRACE_LOG("LTSSM Monitor Worker Thread - Stop");
}


#endif
#endif

///////////////////////////////////////////////////////////////////////////////
// register access functions
///////////////////////////////////////////////////////////////////////////////

inline u32 dct_pcie_ctl_readl(struct dct_pcie *dct_pcie, u32 offset)
{
	return readl(dct_pcie->base + offset);
}

inline void dct_pcie_ctl_writel(struct dct_pcie *dct_pcie, u32 offset, u32 value)
{
	writel(value, dct_pcie->base + offset);
}

inline u32 dct_pcie_dbi_readl(struct dct_pcie *dct_pcie, u32 offset)
{
	return readl(dct_pcie->dbi_base + offset);
}

inline void dct_pcie_dbi_writel(struct dct_pcie *dct_pcie, u32 offset, u32 value)
{
	writel(value, dct_pcie->dbi_base + offset);
}

#define DCT_PCIE_POLLMAX_PMI 10
static int dct_pcie_pmi_writew_quiet(struct dct_pcie *dct_pcie, u8 lane, u32 offset, u16 value)
{
	uint32_t cr_ctrl;
	uint32_t cr_data_addr;
	uint32_t cr_readback;
	int i;

	// static ctrl setup
	cr_ctrl  = 0;
	cr_ctrl  = DCT_PCIE_REGS_FLAG_SET(cr_ctrl, PHY_CR_PARA_SEL);

	// set addr (regard that the we have to program the register index instead of the address)
	// set data
	cr_data_addr = 0;
	cr_data_addr = DCT_PCIE_REGS_FIELD_SET(cr_data_addr, PHY_CR_PARA_ADDR, (offset>>1) );
	cr_data_addr = DCT_PCIE_REGS_FIELD_SET(cr_data_addr, PHY_CR_PARA_WR_DATA, value);
	dct_pcie_ctl_writel(dct_pcie, PCIE_PMI_WR_DA_OFF_REG(lane), cr_data_addr);

	// set wr_en for one cycle
	// loop
	// 		clk up
	// 		clk down
	// while not ack
	cr_readback = 0;
	cr_ctrl  = DCT_PCIE_REGS_FLAG_SET(cr_ctrl, PHY_CR_PARA_WR_EN);
	for(i = 0; (i < DCT_PCIE_POLLMAX_PMI) && !DCT_PCIE_REGS_FLAG_GET(cr_readback, PHY_CR_PARA_ACK); i++) {
		cr_ctrl  = DCT_PCIE_REGS_FLAG_SET(cr_ctrl, PHY_CR_PARA_CLK);
		dct_pcie_ctl_writel(dct_pcie, PCIE_PMI_CTRL_OFF_REG(lane), cr_ctrl);

		cr_ctrl  = DCT_PCIE_REGS_FLAG_CLEAR(cr_ctrl, PHY_CR_PARA_CLK);
		dct_pcie_ctl_writel(dct_pcie, PCIE_PMI_CTRL_OFF_REG(lane), cr_ctrl);

		cr_readback = dct_pcie_ctl_readl(dct_pcie, PCIE_PMI_RD_OFF_REG(lane));
		cr_ctrl = DCT_PCIE_REGS_FLAG_CLEAR(cr_ctrl, PHY_CR_PARA_WR_EN);
	}

	if (i >=  DCT_PCIE_POLLMAX_PMI) {
		return -1;
	} else {
		return 0;
	}
}

void dct_pcie_pmi_writew(struct dct_pcie *dct_pcie, u8 lane, u32 offset, u16 value)
{
	if ( dct_pcie_pmi_writew_quiet(dct_pcie, lane, offset, value) != 0 )
		dev_err(dct_pcie->dw_pcie->dev, "Write PHY address failed");
}

static int dct_pcie_pmi_readw_quiet(struct dct_pcie *dct_pcie, u8 lane, u32 offset, u16 * p_value)
{
	uint32_t cr_ctrl;
	uint32_t cr_data_addr;
	uint32_t cr_readback;
	int i;

	// static ctrl setup
	cr_ctrl  = 0;
	cr_ctrl  = DCT_PCIE_REGS_FLAG_SET(cr_ctrl, PHY_CR_PARA_SEL);

	// set addr (regard that the we have to program the register index instead of the address)
	cr_data_addr = 0;
	cr_data_addr = DCT_PCIE_REGS_FIELD_SET(cr_data_addr, PHY_CR_PARA_ADDR, (offset>>1));
	dct_pcie_ctl_writel(dct_pcie, PCIE_PMI_WR_DA_OFF_REG(lane), cr_data_addr);

	// set rd_en for one cycle
	// loop
	// 		clk up
	// 		clk down
	// while not ack
	cr_readback = 0;
	cr_ctrl  = DCT_PCIE_REGS_FLAG_SET(cr_ctrl, PHY_CR_PARA_RD_EN);
	for(i = 0; (i < DCT_PCIE_POLLMAX_PMI) && !DCT_PCIE_REGS_FLAG_GET(cr_readback, PHY_CR_PARA_ACK); i++) {
		cr_ctrl  = DCT_PCIE_REGS_FLAG_SET(cr_ctrl, PHY_CR_PARA_CLK);
		dct_pcie_ctl_writel(dct_pcie, PCIE_PMI_CTRL_OFF_REG(lane), cr_ctrl);

		cr_ctrl  = DCT_PCIE_REGS_FLAG_CLEAR(cr_ctrl, PHY_CR_PARA_CLK);
		dct_pcie_ctl_writel(dct_pcie, PCIE_PMI_CTRL_OFF_REG(lane), cr_ctrl);

		cr_readback = dct_pcie_ctl_readl(dct_pcie, PCIE_PMI_RD_OFF_REG(lane));
		cr_ctrl  = DCT_PCIE_REGS_FLAG_CLEAR(cr_ctrl, PHY_CR_PARA_RD_EN);
	}

	if (i >=  DCT_PCIE_POLLMAX_PMI) {
		return -1;
	} else {
		*p_value = DCT_PCIE_REGS_FIELD_GET(cr_readback, PHY_CR_PARA_RD_DATA);
		return 0;
	}
}

u16 dct_pcie_pmi_readw(struct dct_pcie *dct_pcie, u8 lane, u32 offset)
{
	uint16_t value = 0;

	if ( dct_pcie_pmi_readw_quiet(dct_pcie, lane, offset, &value) != 0)
		dev_err(dct_pcie->dw_pcie->dev, "Read PHY address failed");
	
	return value;
}

int dct_pcie_pmi_init(struct dct_pcie *dct_pcie, u8 lane)
{
	uint32_t cr_ctrl;
	uint16_t dummy;

	int wiggle = 3;

	// static ctrl setup
	cr_ctrl  = 0;
	cr_ctrl  = DCT_PCIE_REGS_FLAG_SET(cr_ctrl, PHY_CR_PARA_SEL);

	// wiggle the clock
	for(wiggle = 3; wiggle; wiggle--) {
		cr_ctrl  = DCT_PCIE_REGS_FLAG_SET(cr_ctrl, PHY_CR_PARA_CLK);
		dct_pcie_ctl_writel(dct_pcie, PCIE_PMI_CTRL_OFF_REG(lane), cr_ctrl);

		cr_ctrl  = DCT_PCIE_REGS_FLAG_CLEAR(cr_ctrl, PHY_CR_PARA_CLK);
		dct_pcie_ctl_writel(dct_pcie, PCIE_PMI_CTRL_OFF_REG(lane), cr_ctrl);
	}

	//dummy read
	dct_pcie_pmi_readw_quiet(dct_pcie, lane, 0, &dummy);

	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// PCIe link handling
///////////////////////////////////////////////////////////////////////////////

static int dct_pcie_link_is_up(struct dct_pcie *dct_pcie)
{
	struct device *dev = dct_pcie->dw_pcie->dev;

	#define PCIE_DCT_LINK_SMLH_LINK_UP               0x00000001
	#define PCIE_DCT_LINK_RDLH_LINK_UP               0x00000010
	#define PCIE_DCT_LINK_PORT_LOGIC_LINK_UP         0x00000100
	#define PCIE_DCT_LINK_PORT_LOGIC_IN_TRAINING     0x00000200
	#define PCIE_DCT_LINK_UP                         (PCIE_DCT_LINK_SMLH_LINK_UP | PCIE_DCT_LINK_RDLH_LINK_UP | PCIE_DCT_LINK_PORT_LOGIC_LINK_UP)

	uint32_t regval_status3;
	uint32_t regval_status2;
	uint32_t regval_debug1;

	int linkup = 0;

	regval_status3 = dct_pcie_ctl_readl(dct_pcie, PCIE_STATUS3_REG);
	if (DCT_PCIE_REGS_FLAG_GET(regval_status3, SMLH_LINK_UP)) {
		linkup |= PCIE_DCT_LINK_SMLH_LINK_UP;
	}

	regval_status2 = dct_pcie_ctl_readl(dct_pcie, PCIE_STATUS2_REG);
	if (DCT_PCIE_REGS_FLAG_GET(regval_status2, RDLH_LINK_UP)) {
		linkup |= PCIE_DCT_LINK_RDLH_LINK_UP;
	}

	regval_debug1 = dct_pcie_dbi_readl(dct_pcie, PL_DEBUG1_OFF_REG);
	if (DCT_PCIE_REGS_FLAG_GET(regval_debug1, PL_DEBUG1_OFF__LINK_IN_TRAINING)) {
		linkup |= PCIE_DCT_LINK_PORT_LOGIC_IN_TRAINING;
	} else if (DCT_PCIE_REGS_FLAG_GET(regval_debug1, PL_DEBUG1_OFF__LINK_UP)) {
		linkup |= PCIE_DCT_LINK_PORT_LOGIC_LINK_UP;
	}

	if (linkup == PCIE_DCT_LINK_UP) {
		return true;
	} else {
		dev_info(dev, "Link is down: Phys/Data/Port: %s/%s/%s%s%s", 
			DCT_PCIE_REGS_FLAG_GET(regval_status3, SMLH_LINK_UP) ? "UP" : "DOWN",
			DCT_PCIE_REGS_FLAG_GET(regval_status2, RDLH_LINK_UP) ? "UP" : "DOWN",
			DCT_PCIE_REGS_FLAG_GET(regval_debug1, PL_DEBUG1_OFF__LINK_UP) ? "UP" : "DOWN",

			DCT_PCIE_REGS_FLAG_GET(regval_status3, SMLH_LTSSM_STATE_RCVRY_EQ) ? "/EQUALIZATION" : "",
			DCT_PCIE_REGS_FLAG_GET(regval_debug1, PL_DEBUG1_OFF__LINK_IN_TRAINING) ? "/TRAINING" : "");
		return false;
	}
}

static void dct_pcie_link_stop(struct dct_pcie *dct_pcie)
{
	struct device *dev = dct_pcie->dw_pcie->dev;
	uint32_t regval;

	DCT_PCIE_TRACE_LOG("ENTER");

	dev_info(dev, "disable LTSSM\n");
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_CONFIG2_REG);
	regval = DCT_PCIE_REGS_FLAG_CLEAR(regval, APP_LTSSM_ENABLE);
	dct_pcie_ctl_writel(dct_pcie, PCIE_CONFIG2_REG, regval);

#ifdef DCT_PCIE_TRACE_LTSSM_INTERVAL_MS
	dct_pcie_ltssm_monitor_stop(dct_pcie);
#endif

}

static int dct_pcie_link_start(struct dct_pcie *dct_pcie)
{
	struct device *dev = dct_pcie->dw_pcie->dev;
	uint32_t regval;

	DCT_PCIE_TRACE_LOG("ENTER");

	if (dct_pcie_link_is_up(dct_pcie)) {
		dev_info(dev, "link is already up\n");
		return 0;
	}

	// enable LTSSM for link training
	dev_info(dev, "enable LTSSM\n");
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_CONFIG2_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, APP_LTSSM_ENABLE);
	dct_pcie_ctl_writel(dct_pcie, PCIE_CONFIG2_REG, regval);

	// polling for link_up status is implemented manually instead of simply calling the dct_pcie_link_is_up() function
	// to provide options to implement LTSSM debug logs and the Zukimo powerstate quirk
	{
		uint32_t status2;
		uint32_t status3;
		uint32_t debug1;

#ifdef DCT_PCIE_TRACE_LTSSM
		#define LTSSM_SAMPLE_NUM 100
		uint32_t samples_status3[LTSSM_SAMPLE_NUM];
		uint32_t samples_status2[LTSSM_SAMPLE_NUM];
		uint32_t samples_debug1[LTSSM_SAMPLE_NUM];
		int samples = 0;
#endif

		bool link_is_up = false;
		unsigned long timeout = jiffies + msecs_to_jiffies(DCT_PCIE_LINK_UP_POLLMAX_MS);

		while (time_before(jiffies, timeout)) {
			status2 = dct_pcie_ctl_readl(dct_pcie, PCIE_STATUS2_REG);

#ifdef DCT_PCIE_ZUKIMO_POWERSTATE_QUIRK
			// Software workaround copies the according powerstate values
			regval = dct_pcie_ctl_readl(dct_pcie, PCIE_CONFIG3_REG);
			regval = DCT_PCIE_ZUKIMO_POWERSTATE_QUIRK_COPY(status2, regval);
			dct_pcie_ctl_writel(dct_pcie, PCIE_CONFIG3_REG, regval);

			// when if powerstate L0 is reached, check if Link is up 
			if(DCT_PCIE_REGS_FLAG_GET(status2, MAC_PHY_POWERDOWN) == 0) {
#endif

			status3 = dct_pcie_ctl_readl(dct_pcie, PCIE_STATUS3_REG);
			debug1 = dct_pcie_dbi_readl(dct_pcie, PL_DEBUG1_OFF_REG) & (PL_DEBUG1_OFF__LINK_UP_MASK | PL_DEBUG1_OFF__LINK_IN_TRAINING_MASK);

#ifdef DCT_PCIE_TRACE_LTSSM
			if ((samples < (LTSSM_SAMPLE_NUM-1)) &&  ((samples_status2[samples] != status2) || (samples_status3[samples] != status3) || (samples_debug1[samples] != debug1)) ) {
				samples_status3[samples] = samples_status3[samples+1] = status3;
				samples_status2[samples] = samples_status2[samples+1] = status2;
				samples_debug1[samples]  = samples_debug1[samples+1]  = debug1;
				samples++;
			}
#endif
			link_is_up = DCT_PCIE_REGS_FLAG_GET(status3, SMLH_LINK_UP) &&
							DCT_PCIE_REGS_FLAG_GET(status2, RDLH_LINK_UP) &&
							DCT_PCIE_REGS_FLAG_GET(debug1, PL_DEBUG1_OFF__LINK_UP) && 
							!DCT_PCIE_REGS_FLAG_GET(debug1, PL_DEBUG1_OFF__LINK_IN_TRAINING);
			if (link_is_up) {
				break;
			}
#ifdef DCT_PCIE_ZUKIMO_POWERSTATE_QUIRK
			}
#endif
		}

		if (link_is_up) {
			DCT_PCIE_TRACE_LOG("link is up");
			dct_pcie->link.status = DCT_PCIE_LINK_UP;
		} else {
			dev_info(dev, "Link up never reached");
		}

#ifdef DCT_PCIE_TRACE_LTSSM
		DCT_PCIE_TRACE_LTSSM_LOG("sampled values: %d", samples);
		for (int i = 0; i < samples; i++) {
			DCT_PCIE_TRACE_LTSSM_STATE_LOG(samples_status3[i], samples_status2[i], samples_debug1[i]);
		}
#endif
	}

#ifdef DCT_PCIE_TRACE_LTSSM_INTERVAL_MS
	dct_pcie_ltssm_monitor_start(dct_pcie);
#endif

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Callback functions for dw_pcie_ops
///////////////////////////////////////////////////////////////////////////////

static int dct_pcie_ops_link_is_up(struct dw_pcie *dw_pcie)
{
	struct dct_pcie *dct_pcie = dev_get_drvdata(dw_pcie->dev);
	return dct_pcie_link_is_up(dct_pcie);
}

static int dct_pcie_rc_ops_link_start(struct dw_pcie *dw_pcie)
{
	struct dct_pcie *dct_pcie = dev_get_drvdata(dw_pcie->dev);

	DCT_PCIE_TRACE_LOG("PERST de-asserted for device. Starting link training!");
	gpiod_set_value_cansleep(dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn].desc, 1);

	dct_pcie->link.status = DCT_PCIE_LINK_ENABLED;

	return dct_pcie_link_start(dct_pcie);
}

static void dct_pcie_rc_ops_link_stop(struct dw_pcie *dw_pcie)
{
	struct dct_pcie *dct_pcie = dev_get_drvdata(dw_pcie->dev);

	DCT_PCIE_TRACE_LOG("PERST asserted for device. Shutting down the PCIe link!");
	gpiod_set_value_cansleep(dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn].desc, 0);

	dct_pcie->link.status = DCT_PCIE_LINK_DISABLED;

	dct_pcie_link_stop(dct_pcie);
}

static const struct dw_pcie_ops dct_pcie_rc_link_ops = {
	.start_link = dct_pcie_rc_ops_link_start,
	.stop_link = dct_pcie_rc_ops_link_stop,
	.link_up = dct_pcie_ops_link_is_up,
};

static int dct_pcie_ep_ops_link_start(struct dw_pcie *dw_pcie)
{
	struct dct_pcie *dct_pcie = dev_get_drvdata(dw_pcie->dev);

	dct_pcie->link.status = DCT_PCIE_LINK_ENABLED;

	enable_irq(dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn].irq);

	return 0;
}

static void dct_pcie_ep_ops_link_stop(struct dw_pcie *dw_pcie)
{
	struct dct_pcie *dct_pcie = dev_get_drvdata(dw_pcie->dev);

	dct_pcie->link.status = DCT_PCIE_LINK_DISABLED;
	
	disable_irq(dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn].irq);

	return;
}

static const struct dw_pcie_ops dct_pcie_ep_link_ops = {
	.start_link = dct_pcie_ep_ops_link_start,
	.stop_link = dct_pcie_ep_ops_link_stop,
	.link_up = dct_pcie_ops_link_is_up,
};



///////////////////////////////////////////////////////////////////////////////
// PCIe Root Complex interrupt handling functions
///////////////////////////////////////////////////////////////////////////////

static int dct_pcie_rc_intx_map(struct irq_domain *domain, unsigned int irq,
				irq_hw_number_t hwirq)
{
	DCT_PCIE_TRACE_LOG("ENTER");

	irq_set_chip_and_handler(irq, &dummy_irq_chip, handle_simple_irq);
	irq_set_chip_data(irq, domain->host_data);

	return 0;
}

static const struct irq_domain_ops intx_domain_ops = {
	.map = dct_pcie_rc_intx_map,
	.xlate = pci_irqd_intx_xlate,
};

static int dct_pcie_rc_handle_msi(struct dw_pcie_rp *dw_pcie_rp, int index)
{
	struct dw_pcie *dw_pcie = to_dw_pcie_from_pp(dw_pcie_rp);
	unsigned long val;
	int pos;

	DCT_PCIE_TRACE_LOG("ENTER");

	val = dw_pcie_readl_dbi(dw_pcie, PCIE_MSI_INTR0_STATUS + (index * MSI_REG_CTRL_BLOCK_SIZE));
	if (!val)
		return 0;

	pos = find_first_bit(&val, MAX_MSI_IRQS_PER_CTRL);
	while (pos != MAX_MSI_IRQS_PER_CTRL) {
		generic_handle_domain_irq(dw_pcie_rp->irq_domain,
					  (index * MAX_MSI_IRQS_PER_CTRL) + pos);
		pos++;
		pos = find_next_bit(&val, MAX_MSI_IRQS_PER_CTRL, pos);
	}

	return 1;
}

static void dct_pcie_rc_handle_msi_irq(struct dw_pcie_rp *dw_pcie_rp)
{
	struct dw_pcie *dw_pcie = to_dw_pcie_from_pp(dw_pcie_rp);
	int ret, i, count, num_ctrls;

	DCT_PCIE_TRACE_LOG("ENTER");

	num_ctrls = dw_pcie_rp->num_vectors / MAX_MSI_IRQS_PER_CTRL;

	/**
	 * Need to make sure all MSI status bits read 0 before exiting.
	 * Else, new MSI IRQs are not registered by the wrapper. Have an
	 * upperbound for the loop and exit the IRQ in case of IRQ flood
	 * to avoid locking up system in interrupt context.
	 */
	count = 0;
	do {
		ret = 0;

		for (i = 0; i < num_ctrls; i++)
			ret |= dct_pcie_rc_handle_msi(dw_pcie_rp, i);
		count++;
	} while (ret && count <= 1000);

	if (count > 1000)
		dev_warn_ratelimited(dw_pcie->dev,
				     "Too many MSI IRQs to handle\n");
}



///////////////////////////////////////////////////////////////////////////////
// PCIe Endpoint interrupt handling functions
///////////////////////////////////////////////////////////////////////////////

static void dct_pcie_ep_raise_legacy_irq(struct dct_pcie *dct_pcie)
{
	DCT_PCIE_TRACE_LOG("ENTER");
}

static void dct_pcie_ep_raise_msi_irq(struct dct_pcie *dct_pcie, u8 interrupt_num)
{
	DCT_PCIE_TRACE_LOG("ENTER");
}

static int dct_pcie_ep_raise_irq(struct dw_pcie_ep *dw_pcie_ep, u8 func_no, enum pci_epc_irq_type type, u16 interrupt_num)
{
	struct dw_pcie *dw_pcie = to_dw_pcie_from_ep(dw_pcie_ep);
	struct dct_pcie *dct_pcie = dev_get_drvdata(dw_pcie->dev);

	DCT_PCIE_TRACE_LOG("ENTER");
	
	switch (type) {
	case PCI_EPC_IRQ_LEGACY:
		dct_pcie_ep_raise_legacy_irq(dct_pcie);
		break;
	case PCI_EPC_IRQ_MSI:
		dct_pcie_ep_raise_msi_irq(dct_pcie, interrupt_num);
		break;
	default:
		dev_err(dw_pcie->dev, "UNKNOWN IRQ type\n");
	}

	return 0;
}

static const struct pci_epc_features dct_pcie_epc_features = {
	// indicate if the EPC device can notify EPF driver on link up
	#ifdef DCT_PCIE_ZUKIMO_CONFIG_LINK_UP_NOTIFIER
		.linkup_notifier = true,
	#else
		.linkup_notifier = false,
	#endif
	// indicate cores that can notify about their availability for initialization
	#ifdef DCT_PCIE_ZUKIMO_CONFIG_CORE_INIT_NOTIFIER
		.core_init_notifier = true,
	#else
		.core_init_notifier = false,
	#endif
	.msi_capable = false,          // indicate if the endpoint function has MSI capability
	.msix_capable = false,         // indicate if the endpoint function has MSI-X capability
	.reserved_bar = 0x28,          // bitmap to indicate reserved BAR unavailable to function driver
	.bar_fixed_64bit = 0x01,       // bitmap to indicate fixed 64bit BARs - adjusted to 3 64bit bars
	.align = 64 * 1024,            // alignment size required for BAR buffer allocation 
	// Array specifying the size supported by each BAR
	.bar_fixed_size[BAR_0] = 0x100000, // the first 64bit bar is fixed to 1MB
	.bar_fixed_size[BAR_1] = 0,        // upper part of the 64bit address
	.bar_fixed_size[BAR_2] = 0x100000, // the second 32bit bar is fixed to 1MB
	.bar_fixed_size[BAR_3] = 0,		   // this bar register is reserved
	.bar_fixed_size[BAR_4] = 0x100,    // the third 32bit bar is fixed to 256 Bytes
	.bar_fixed_size[BAR_5] = 0,        // this bar register is reserved
};

static const struct pci_epc_features* dct_pcie_ep_get_features(struct dw_pcie_ep *dw_pcie_ep)
{
	DCT_PCIE_TRACE_LOG("ENTER");

	return &dct_pcie_epc_features;
}



///////////////////////////////////////////////////////////////////////////////
// PCIe IP core interrupt handling
///////////////////////////////////////////////////////////////////////////////

static void dct_pcie_core_irq_enable(struct dct_pcie *dct_pcie)
{
	DCT_PCIE_TRACE_LOG("ENTER");

	dct_pcie_ctl_writel(dct_pcie, PCIE_ISC_REG, 0xffffffff);
	dct_pcie_ctl_writel(dct_pcie, PCIE_ISM_REG, PCIE_IRQ_MASK);

	dct_pcie_ctl_writel(dct_pcie, PCIE_ERROR_ISC_REG, 0xffffffff);
	dct_pcie_ctl_writel(dct_pcie, PCIE_ERROR_ISM_REG, PCIE_IRQERR_MASK);
}

static void dct_pcie_core_irq_disable(struct dct_pcie *dct_pcie)
{
	DCT_PCIE_TRACE_LOG("ENTER");

	dct_pcie_ctl_writel(dct_pcie, PCIE_ISM_REG, 0x00000000);
	dct_pcie_ctl_writel(dct_pcie, PCIE_ISC_REG, 0xffffffff);

	dct_pcie_ctl_writel(dct_pcie, PCIE_ERROR_ISM_REG, 0x00000000);
	dct_pcie_ctl_writel(dct_pcie, PCIE_ERROR_ISC_REG, 0xffffffff);
}

static irqreturn_t dct_pcie_core_irq_ctl_handler(int irq, void *arg)
{
	struct dct_pcie *dct_pcie = arg;
	struct dw_pcie *dw_pcie = dct_pcie->dw_pcie;
	struct device *dev = dw_pcie->dev;
	u32 reg;

	DCT_PCIE_TRACE_LOG("ENTER");

	reg = dct_pcie_ctl_readl(dct_pcie, PCIE_MIS_REG);

	dct_pcie_ctl_writel(dct_pcie, PCIE_ISC_REG, reg);

	if (reg & PCIE_IRQ_WAKE) {
		dev_err(dev, "IRQ WAKE\n");
	}
	if (reg & PCIE_IRQ_HP_PME) {
		dev_err(dev, "IRQ HP_PME\n");
	}
	if (reg & PCIE_IRQ_CFG_PME) {
		dev_err(dev, "IRQ CFG_PME\n");
	}
	if (reg & PCIE_IRQ_CFG_LINK_EQ_REQ) {
		dev_err(dev, "IRQ CFG_LINK_EQ_REQ\n");
	}
	if (reg & PCIE_IRQ_CFG_LINK_AUTO_BW) {
		dev_err(dev, "IRQ CFG_LINK_AUTO_BW\n");
	}
	if (reg & PCIE_IRQ_CFG_BW_MGT) {
		dev_err(dev, "IRQ CFG_BW_MGT\n");
	}
	if (reg & PCIE_IRQ_MSI_CTRL) {
		if (dct_pcie->mode == DW_PCIE_RC_TYPE) {
			struct dw_pcie_rp *dw_pcie_rp = &dw_pcie->pp;
			dct_pcie_rc_handle_msi_irq(dw_pcie_rp);
		}
	}
	return IRQ_HANDLED;
}

static irqreturn_t dct_pcie_core_irq_err_handler(int irq, void *arg)
{
	struct dct_pcie *dct_pcie = arg;
	struct dw_pcie *dw_pcie = dct_pcie->dw_pcie;
	struct device *dev = dw_pcie->dev;
	u32 reg;

	DCT_PCIE_TRACE_LOG("ENTER");

	reg = dct_pcie_ctl_readl(dct_pcie, PCIE_ERROR_MIS_REG);

	dct_pcie_ctl_writel(dct_pcie, PCIE_ERROR_ISC_REG, reg);

	if (reg & PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_0) {
		dev_err(dev, "IRQ ERR RADM_TRGT1_ATU_CBUF_0\n");
	}
	if (reg & PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_1) {
		dev_err(dev, "IRQ ERR RADM_TRGT1_ATU_CBUF_1\n");
	}
	if (reg & PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_2) {
		dev_err(dev, "IRQ ERR RADM_TRGT1_ATU_CBUF_2\n");
	}
	if (reg & PCIE_IRQERR_RADM_TRGT1_ATU_CBUF_3) {
		dev_err(dev, "IRQ ERR RADM_TRGT1_ATU_CBUF_3\n");
	}
	if (reg & PCIE_IRQERR_RADM_NONFATAL) {
		dev_err(dev, "IRQ ERR RADM_NONFATAL\n");
	}
	if (reg & PCIE_IRQERR_RADM_FATAL) {
		dev_err(dev, "IRQ ERR RADM_FATAL\n");
	}
	if (reg & PCIE_IRQERR_RADM_CPL_TIMEOUT) {
		dev_err(dev, "IRQ ERR RADM_CPL_TIMEOUT\n");
	}
	if (reg & PCIE_IRQERR_RADM_CORRECTABLE) {
		dev_err(dev, "IRQ ERR RADM_CORRECTABLE\n");
	}
	if (reg & PCIE_IRQERR_IF_TIMEOUT_STATUS) {
		dev_err(dev, "IRQ ERR IF_TIMEOUT_STATUS\n");
	}
	if (reg & PCIE_IRQERR_CFG_UNCOR_INTERNAL_STS) {
		dev_err(dev, "IRQ ERR CFG_UNCOR_INTERNAL_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_REPLAY_TIMER_TIMEOUT_STS) {
		dev_err(dev, "IRQ ERR CFG_REPLAY_TIMER_TIMEOUT_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_REPLAY_NUMBER_ROLLOVER_STS) {
		dev_err(dev, "IRQ ERR CFG_REPLAY_NUMBER_ROLLOVER_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_RCVR_OVERFLOW_STS) {
		dev_err(dev, "IRQ ERR CFG_RCVR_OVERFLOW_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_RCVR_STS) {
		dev_err(dev, "IRQ ERR CFG_RCVR_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_MLF_TLP_STS) {
		dev_err(dev, "IRQ ERR CFG_MLF_TLP_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_FC_PROTOCOL_STS) {
		dev_err(dev, "IRQ ERR CFG_FC_PROTOCOL_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_ECRC_STS) {
		dev_err(dev, "IRQ ERR CFG_ECRC_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_DL_PROTOCOL_STS) {
		dev_err(dev, "IRQ ERR CFG_DL_PROTOCOL_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_CORRECTED_INTERNAL_STS) {
		dev_err(dev, "IRQ ERR CFG_CORRECTED_INTERNAL_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_BAD_TLP_STS) {
		dev_err(dev, "IRQ ERR CFG_BAD_TLP_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_BAD_DLLP_STS) {
		dev_err(dev, "IRQ ERR CFG_BAD_DLLP_STS\n");
	}
	if (reg & PCIE_IRQERR_CFG_AER_RC) {
		dev_err(dev, "IRQ ERR CFG_AER_RC\n");
	}
	if (reg & PCIE_IRQERR_AXI_PCIE_TX_RASDP_MODE) {
		dev_err(dev, "IRQ ERR AXI_PCIE_TX_RASDP_MODE\n");
	}
	if (reg & PCIE_IRQERR_AXI_PCIE_RX_RASDP_MODE) {
		dev_err(dev, "IRQ ERR AXI_PCIE_RX_RASDP_MODE\n");
	}
	if (reg & PCIE_IRQERR_APP_PARITY) {
		dev_err(dev, "IRQ ERR APP_PARITY\n");
	}

	return IRQ_HANDLED;
}



///////////////////////////////////////////////////////////////////////////////
// General PCIe EP IP core functions
///////////////////////////////////////////////////////////////////////////////

static void dct_pcie_ep_init(struct dw_pcie_ep *dw_pcie_ep)
{
	struct dw_pcie *dw_pcie = to_dw_pcie_from_ep(dw_pcie_ep);
	struct dct_pcie *dct_pcie = dev_get_drvdata(dw_pcie->dev);
	enum pci_barno bar;

	DCT_PCIE_TRACE_LOG("ENTER");

	for (bar = 0; bar < PCI_STD_NUM_BARS; bar++)
		dw_pcie_ep_reset_bar(dw_pcie, bar);

	dct_pcie_core_irq_enable(dct_pcie);
}

static const struct dw_pcie_ep_ops dct_pcie_ep_ops = {
	.ep_init = dct_pcie_ep_init,
	.raise_irq = dct_pcie_ep_raise_irq,
	.get_features = dct_pcie_ep_get_features,
};

static void dct_pcie_ep_core_disable(struct dct_pcie *dct_pcie, struct platform_device *pdev)
{
	struct dw_pcie *dw_pcie = dct_pcie->dw_pcie;
	struct dw_pcie_ep *dw_pcie_ep = &dw_pcie->ep;

	DCT_PCIE_TRACE_LOG("ENTER");

	dw_pcie_ep_exit(dw_pcie_ep);

	dct_pcie_core_disable(dct_pcie);
}

static int dct_pcie_ep_core_enable(struct dct_pcie *dct_pcie, struct platform_device *pdev)
{
	int ret;
	struct device *dev = &pdev->dev;
	struct dw_pcie *dw_pcie = dct_pcie->dw_pcie;
	struct dw_pcie_ep *dw_pcie_ep = &dw_pcie->ep;

	DCT_PCIE_TRACE_LOG("ENTER");
	
	dw_pcie_ep->ops = &dct_pcie_ep_ops;

	ret = dw_pcie_ep_init(dw_pcie_ep);
	if (ret) {
		dev_err(dev, "failed to initialize endpoint\n");
		return ret;
	}

	return 0;
}

static void dct_pcie_core_ep_perst_deassert(struct dct_pcie *dct_pcie)
{
	int ret;
	struct device *dev = dct_pcie->dw_pcie->dev;
	struct dw_pcie *dw_pcie = dct_pcie->dw_pcie;
	struct dw_pcie_ep *dw_pcie_ep = &dw_pcie->ep;
	enum pci_barno bar;

	DCT_PCIE_TRACE_LOG("ENTER");

	// enable the pcie core
	ret = dct_pcie_core_enable(dct_pcie);
	if (ret) {
		dev_err(dev, "failed to enable pcie core\n");
		return;
	}

	for (bar = 0; bar < PCI_STD_NUM_BARS; bar++)
		dw_pcie_ep_reset_bar(dw_pcie, bar);

	// complete the ep initialization when pcie core is enabled
	ret = dw_pcie_ep_init_complete(dw_pcie_ep);
	if (ret) {
		dev_err(dev, "Failed to complete initialization: %d\n", ret);
		return;
	}

	// call the core init notification
#ifdef DCT_PCIE_ZUKIMO_CONFIG_CORE_INIT_NOTIFIER
	DCT_PCIE_TRACE_LOG("call the core init notification");
	dw_pcie_ep_init_notify(dw_pcie_ep);
#endif

	// start the link
	dct_pcie_link_start(dct_pcie);

#ifdef DCT_PCIE_ZUKIMO_CONFIG_LINK_UP_NOTIFIER
	// call the link up notification
	if( dct_pcie->link.status == DCT_PCIE_LINK_UP ) {
		DCT_PCIE_TRACE_LOG("call the link up notification");
		dw_pcie_ep_linkup(dw_pcie_ep);
	}
#endif
}

static void dct_pcie_core_ep_perst_assert(struct dct_pcie *dct_pcie)
{
	int ret;
	struct device *dev = dct_pcie->dw_pcie->dev;

	DCT_PCIE_TRACE_LOG("ENTER");

	dct_pcie->link.status = DCT_PCIE_LINK_DISABLED;

	// when pcie reset is asserted no refclk might be avaiale
	// access to the pcie controller might fail
	// disable the pcie core with brute force
	dct_pcie_core_disable(dct_pcie);
}

static irqreturn_t dct_pcie_core_ep_irq_perst_handler(int irq, void *arg)
{
	struct dct_pcie *dct_pcie = arg;
	struct dw_pcie *dw_pcie = dct_pcie->dw_pcie;
	struct device *dev = dw_pcie->dev;

	int perst = gpiod_get_raw_value_cansleep(dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn].desc);

	if (perst) {
		DCT_PCIE_TRACE_LOG("PERST de-asserted by host. Starting link training!");
		dct_pcie_core_ep_perst_deassert(dct_pcie);
	} else {
		DCT_PCIE_TRACE_LOG("PERST asserted by host. Shutting down the PCIe link!");
		dct_pcie_core_ep_perst_assert(dct_pcie);
	}

	irq_set_irq_type(dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn].irq, (perst ? IRQF_TRIGGER_FALLING : IRQF_TRIGGER_RISING));

	return IRQ_HANDLED;
}



///////////////////////////////////////////////////////////////////////////////
// General PCIe RC IP core functions
///////////////////////////////////////////////////////////////////////////////

static int dct_pcie_rc_init(struct dw_pcie_rp *dw_pcie_rp)
{
	struct dw_pcie *dw_pcie = to_dw_pcie_from_pp(dw_pcie_rp);
	struct dct_pcie *dct_pcie = dev_get_drvdata(dw_pcie->dev);

	DCT_PCIE_TRACE_LOG("ENTER");

	dct_pcie_core_irq_enable(dct_pcie);

	return 0;
}

static const struct dw_pcie_host_ops dct_pcie_rc_ops = {
	.host_init = dct_pcie_rc_init,
};

static void dct_pcie_rc_core_disable(struct dct_pcie *dct_pcie, struct platform_device *pdev)
{
	struct dw_pcie *dw_pcie = dct_pcie->dw_pcie;
	struct dw_pcie_rp *dw_pcie_rp = &dw_pcie->pp;

	DCT_PCIE_TRACE_LOG("ENTER");

	dw_pcie_host_deinit(dw_pcie_rp);

	dct_pcie_core_disable(dct_pcie);
}

static int dct_pcie_rc_core_enable(struct dct_pcie *dct_pcie, struct platform_device *pdev)
{
	int ret;
	struct device *dev = &pdev->dev;
	struct dw_pcie *dw_pcie = dct_pcie->dw_pcie;
	struct dw_pcie_rp *dw_pcie_rp = &dw_pcie->pp;

	DCT_PCIE_TRACE_LOG("ENTER");

	ret = dct_pcie_core_enable(dct_pcie);
	if (ret) {
		dev_err(dev, "failed to enable pcie core\n");
		return ret;
	}

	dw_pcie_rp->irq = platform_get_irq(pdev, 1);
	if (dw_pcie_rp->irq < 0)
		return dw_pcie_rp->irq;

	/* MSI IRQ is muxed */
	dw_pcie_rp->msi_irq[0] = -ENODEV;

	dw_pcie_rp->ops = &dct_pcie_rc_ops;

	ret = dw_pcie_host_init(dw_pcie_rp);
	if (ret) {
		dev_err(dev, "failed to initialize host\n");
		return ret;
	}

	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// Common PCIe IP core functions
///////////////////////////////////////////////////////////////////////////////

static void dct_pcie_core_disable(struct dct_pcie *dct_pcie)
{
	DCT_PCIE_TRACE_LOG("ENTER");

#ifdef DCT_PCIE_TRACE_LTSSM_INTERVAL_MS
	dct_pcie_ltssm_monitor_stop(dct_pcie);
#endif

	// assert all resets and reset all clocks
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, 0);
	dct_pcie_ctl_writel(dct_pcie, PCIE_EN_CLK_REG, 0);
}

static int dct_pcie_core_enable(struct dct_pcie *dct_pcie)
{
	struct device *dev = dct_pcie->dw_pcie->dev;

	uint32_t regval;
	int lane_mask;
	int i;

	const bool sram_bypass = 1;
	const bool sram_ext_ld_done = 0;

	DCT_PCIE_TRACE_LOG("ENTER");

	if (dct_pcie->link.num_lanes > 3) {
		lane_mask = 0x0f;
	} else if (dct_pcie->link.num_lanes > 1) {
		lane_mask = 0x03;
	} else {
		lane_mask = 0x01;
	}

	// assert all resets and reset all clocks
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, 0);
	dct_pcie_ctl_writel(dct_pcie, PCIE_EN_CLK_REG, 0);

	// set up core and device type
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_CONFIG1_REG);
	if (dct_pcie->mode == DW_PCIE_RC_TYPE) {
		regval = DCT_PCIE_REGS_FLAG_SET(regval, DEVICE_TYPE);
	} else {
		regval = DCT_PCIE_REGS_FLAG_CLEAR(regval, DEVICE_TYPE);
	}
	regval = DCT_PCIE_REGS_FLAG_CLEAR(regval, PHY_MPLL_SSC_EN);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, PHY_PCS_PWR_STABLE, 0xf);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, PHY_PMA_PWR_STABLE, 0xf);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, UPCS_PWR_STABLE);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, PHY_EXT_CTRL_SEL);
	dct_pcie_ctl_writel(dct_pcie, PCIE_CONFIG1_REG, regval);

	// MPLLA configure for Gen1/Gen2
	regval = 0;
	regval = DCT_PCIE_REGS_FLAG_SET(regval, MPLLA_DIV5_CLK_EN);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, MPLLA_CP_INT, 20);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, MPLLA_CP_PROP, 63);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, MPLLA_MULTIPLIER, 18);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, REF_CLK_MPLLA_DIV, 0);
	dct_pcie_ctl_writel(dct_pcie, PCIE_MPLLA_REG, regval);

	// MPLLA disable spread spectrum for Gen1/Gen2
	regval = 0;
	dct_pcie_ctl_writel(dct_pcie, PCIE_MPLLA_SSC1_REG, 0);
	dct_pcie_ctl_writel(dct_pcie, PCIE_MPLLA_SSC2_REG, 0);

	// MPLLB configure for Gen3
	regval = 0;
	regval = DCT_PCIE_REGS_FLAG_CLEAR(regval, MPLLB_DIV5_CLK_EN);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, MPLLB_CP_INT, 7);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, MPLLB_CP_PROP, 37);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, MPLLB_MULTIPLIER, 128);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, REF_CLK_MPLLB_DIV, 1);
	dct_pcie_ctl_writel(dct_pcie, PCIE_MPLLB_REG, regval);

	// MPLLB disable spread spectrum for Gen3
	regval = 0;
	dct_pcie_ctl_writel(dct_pcie, PCIE_MPLLB_SSC1_REG, 0);
	dct_pcie_ctl_writel(dct_pcie, PCIE_MPLLB_SSC2_REG, 0);

	// set SRAM_BYPASS/SRAM_EXT_LD_DONE
	// we are using the default value:
	// sram_bypass = 1, sram_ext_ld_done = 0
	regval = 0;
	regval |= sram_bypass ? (lane_mask << 0) : 0;
	regval |= sram_ext_ld_done ? (lane_mask << 8) : 0;
	dct_pcie_ctl_writel(dct_pcie, PCIE_FW_RAM_CTRL_REG, regval);

	// release CTL resets
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_CTL_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	// release PHY resets
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_PHY_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	// release PIPE_LANE resets
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_PIPE_LANE_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	// enable DBI clock
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_EN_CLK_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_EN_CLK_AXI_PCIE_DBI);
	dct_pcie_ctl_writel(dct_pcie, PCIE_EN_CLK_REG, regval);

	// release DBI reset
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_AXI_PCIE_DBI_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	// set default values for equilization
	// enable writing readonly dbi register
	regval = dct_pcie_dbi_readl(dct_pcie, MISC_CONTROL_1_OFF_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, MISC_CONTROL_1_OFF__DBI_RO_WR_EN);
	dct_pcie_dbi_writel(dct_pcie, MISC_CONTROL_1_OFF_REG, regval);
	// default values represents an illegal preset - set a legal eq preset 0
	dct_pcie_dbi_writel(dct_pcie, SPCIE_CAP_OFF_0CH_REG_REG, 0);
	// FOM preset0 gen3_eq_pset_req_vec as recommended by pcie spec - enable preset 9,8 and 6,5,4 -> 0x0370
	regval = dct_pcie_dbi_readl(dct_pcie, GEN3_EQ_CONTROL_OFF_REG);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, GEN3_EQ_CONTROL_OFF__GEN3_EQ_PSET_REQ_VEC, 0x0370);
	dct_pcie_dbi_writel(dct_pcie, GEN3_EQ_CONTROL_OFF_REG, regval);
	// disable writing readonly dbi register again
	regval = dct_pcie_dbi_readl(dct_pcie, MISC_CONTROL_1_OFF_REG);
	regval = DCT_PCIE_REGS_FLAG_CLEAR(regval, MISC_CONTROL_1_OFF__DBI_RO_WR_EN);
	dct_pcie_dbi_writel(dct_pcie, MISC_CONTROL_1_OFF_REG, regval);

	// disable elastic buffer mode
	regval = dct_pcie_dbi_readl(dct_pcie, LANE_SKEW_OFF_REG);
	regval = DCT_PCIE_REGS_FLAG_CLEAR(regval, LANE_SKEW_OFF__ELASTIC_BUFFER_MODE);
	dct_pcie_dbi_writel(dct_pcie, LANE_SKEW_OFF_REG, regval);

	// set transmit margin
	regval = dct_pcie_dbi_readl(dct_pcie, LINK_CONTROL2_LINK_STATUS2_REG_REG);
	regval = DCT_PCIE_REGS_FIELD_SET(regval, LINK_CONTROL2_LINK_STATUS2_REG__PCIE_CAP_TX_MARGIN, 2);
	dct_pcie_dbi_writel(dct_pcie, LINK_CONTROL2_LINK_STATUS2_REG_REG, regval);

#if 0
	// controller loopback
	// controller databook, section C.2.1
	DCT_PCIE_TRACE_LOG("loopback: local controller loopback");

	regval = dct_pcie_dbi_readl(dct_pcie, GEN3_RELATED_OFF_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, GEN3_RELATED_OFF__GEN3_EQUALIZATION_DISABLE);
	dct_pcie_dbi_writel(dct_pcie, GEN3_RELATED_OFF_REG, regval);

	regval = dct_pcie_dbi_readl(dct_pcie, PIPE_LOOPBACK_CONTROL_OFF_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, PIPE_LOOPBACK_CONTROL_OFF__PIPE_LOOPBACK);
	dct_pcie_dbi_writel(dct_pcie, PIPE_LOOPBACK_CONTROL_OFF_REG, regval);

	regval = dct_pcie_dbi_readl(dct_pcie, PORT_LINK_CTRL_OFF_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, PORT_LINK_CTRL_OFF__LOOPBACK_ENABLE);
	dct_pcie_dbi_writel(dct_pcie, PORT_LINK_CTRL_OFF_REG, regval);
#endif

#if 0
	// phy loopback
	// phy databook section 6.18.7
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_CONFIG3_REG);
#if 0
	DCT_PCIE_TRACE_LOG("loopback: local phy loopback");
	regval = DCT_PCIE_REGS_FLAG_SET(regval, PIPE_LANE_TX2RX_LOOPBK);
#else
	DCT_PCIE_TRACE_LOG("loopback: remote phy loopback");
	regval = DCT_PCIE_REGS_FLAG_CLEAR(regval, PIPE_LANE_TX2RX_LOOPBK);
#endif
	dct_pcie_ctl_writel(dct_pcie, PCIE_CONFIG3_REG, regval);

	regval = dct_pcie_dbi_readl(dct_pcie, PORT_LINK_CTRL_OFF_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, PORT_LINK_CTRL_OFF__LOOPBACK_ENABLE);
	dct_pcie_dbi_writel(dct_pcie, PORT_LINK_CTRL_OFF_REG, regval);
#endif

	// enable PCIe TX/RX clock
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_EN_CLK_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_EN_CLK_AXI_PCIE_TX);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_EN_CLK_AXI_PCIE_RX);
	dct_pcie_ctl_writel(dct_pcie, PCIE_EN_CLK_REG, regval);

#ifdef DCT_PCIE_ZUKIMO_POWERSTATE_QUIRK
	// initial copy the powerstate after reset
	{
		uint32_t status2 = dct_pcie_ctl_readl(dct_pcie, PCIE_STATUS2_REG);
		uint32_t config3 = dct_pcie_ctl_readl(dct_pcie, PCIE_CONFIG3_REG);
		uint32_t regval = DCT_PCIE_ZUKIMO_POWERSTATE_QUIRK_COPY(status2, config3);
		dct_pcie_ctl_writel(dct_pcie, PCIE_CONFIG3_REG, regval);
	}
#endif

	// release PCIe TX/RX reset
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_AXI_PCIE_TX_N);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_AXI_PCIE_RX_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	// init pmi interface lane
	for (i=0; i<dct_pcie->link.num_lanes; i++) {
		dct_pcie_pmi_init(dct_pcie, i);
	}

#if 0
	// This code is left here in case there are problems with PLL locking
	// poll MPLLA lock bit (only sup block phy 0)
	# define DCT_PCIE_POLLMAX_MPLLA     100
	regval = dct_pcie_pmi_readw(dct_pcie, 0, SUP_DIG_MPLLA_MPLL_PWR_CTL_STAT_REG);
	for(i = 0; (i < DCT_PCIE_POLLMAX_MPLLA) && !DCT_PCIE_REGS_FLAG_GET(regval, SUP_DIG_MPLLA_MPLL_PWR_CTL_STAT__MPLL_LOCK); i++) {
		regval = dct_pcie_pmi_readw(dct_pcie, 0, SUP_DIG_MPLLA_MPLL_PWR_CTL_STAT_REG);
	}
	if (i >=  DCT_PCIE_POLLMAX_MPLLA) {
		dev_err(dev, "poll MPLLA failed");
		goto err_core;
	}
	DCT_PCIE_TRACE_LOG("MPLLA - %d - 0x%04x - %s%s", i, regval,
                                                 DCT_PCIE_REGS_FLAG_GET(regval, SUP_DIG_MPLLA_MPLL_PWR_CTL_STAT__MPLL_LOCK) ? "locked" : "NOT locked",
                                                 (i >=  DCT_PCIE_POLLMAX_MPLLA) ? " - timeout" : "");
#endif

#if 0
	// This code is left here in case there are problems with PLL locking
	// poll MPLLB lock bit (only sup block phy 0)
	# define DCT_PCIE_POLLMAX_MPLLB     100
	regval = dct_pcie_pmi_readw(dct_pcie, 0, SUP_DIG_MPLLB_MPLL_PWR_CTL_STAT_REG);
	for(i = 0; (i <  DCT_PCIE_POLLMAX_MPLLB) && !DCT_PCIE_REGS_FLAG_GET(regval, SUP_DIG_MPLLB_MPLL_PWR_CTL_STAT__MPLL_LOCK); i++) {
		regval = dct_pcie_pmi_readw(dct_pcie, 0, SUP_DIG_MPLLB_MPLL_PWR_CTL_STAT_REG);
	}
	if (i >= DCT_PCIE_POLLMAX_MPLLB) {
		dev_err(dev, "poll MPLLB failed");
		goto err_core;
	}
	DCT_PCIE_TRACE_LOG("MPLLB - %d - 0x%04x - %s%s", i, regval,
                                                 DCT_PCIE_REGS_FLAG_GET(regval, SUP_DIG_MPLLB_MPLL_PWR_CTL_STAT__MPLL_LOCK) ? "locked" : "NOT locked",
                                                 (i >= DCT_PCIE_POLLMAX_MPLLB) ? " - timeout" : "");
#endif

	// poll SRAM_INIT_DONE
	# define DCT_PCIE_POLLMAX_SRAM_INIT_DONE     100
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_FW_RAM_STAT_REG);
	for(i = 0; (i <  DCT_PCIE_POLLMAX_SRAM_INIT_DONE) && ((regval & lane_mask) != lane_mask); i++) {
		regval = dct_pcie_ctl_readl(dct_pcie, PCIE_FW_RAM_STAT_REG);
	}
	if (i >= DCT_PCIE_POLLMAX_SRAM_INIT_DONE) {
		dev_err(dct_pcie->dw_pcie->dev, "poll SRAM_INIT_DONE failed\n");
		goto err_core;
	}
	DCT_PCIE_TRACE_LOG("SRAM_INIT_DONE - %d - 0x%04x - %s%s", i, regval,
                                                              (regval == lane_mask) ? "done" : "NOT done",
                                                              (i >= DCT_PCIE_POLLMAX_SRAM_INIT_DONE) ? " - timeout" : "");

	// poll TxX/RxX Ack
	# define DCT_PCIE_POLLMAX_RX_TX_ACK     100
	regval = 0;
	for(i = 0; (i <  DCT_PCIE_POLLMAX_RX_TX_ACK) && (regval != (lane_mask<<8 | lane_mask)); i++) {
		int l;
		uint32_t val;
		for (l=0; l<dct_pcie->link.num_lanes; l++) {
			val = dct_pcie_pmi_readw(dct_pcie, l, LANEX_DIG_ASIC_TX_ASIC_OUT_REG);
			regval |= !DCT_PCIE_REGS_FLAG_GET(val, LANEX_DIG_ASIC_TX_ASIC_OUT__TX_ACK)
						? 0x0001 << (0 + l) : 0x0000;
		}
		DCT_PCIE_TRACE_LOG("TxX Ack - %d - 0x%04x - %s%s", i, regval, (((regval>>0) & lane_mask) == lane_mask) ? "done" : "NOT done", (i >= DCT_PCIE_POLLMAX_RX_TX_ACK) ? " - timeout" : "");

		for (l=0; l<dct_pcie->link.num_lanes; l++) {
			val = dct_pcie_pmi_readw(dct_pcie, l, LANEX_DIG_ASIC_RX_ASIC_OUT_0_REG);
			regval |= !DCT_PCIE_REGS_FLAG_GET(val, LANEX_DIG_ASIC_RX_ASIC_OUT_0__ACK)
						? 0x0001 << (8 + l) : 0x0000;
		}
		DCT_PCIE_TRACE_LOG("RxX Ack - %d - 0x%04x - %s%s", i, regval, (((regval>>8) & lane_mask) == lane_mask) ? "done" : "NOT done", (i >= DCT_PCIE_POLLMAX_RX_TX_ACK) ? " - timeout" : "");
	}
	if (i >= DCT_PCIE_POLLMAX_RX_TX_ACK) {
		dev_err(dev, "poll TxX/RxX Ack failed");
		goto err_core;
	}

	return 0;

err_core:
	dev_err(dev, "PCIe core enable failed\n");

	return -1;
}



///////////////////////////////////////////////////////////////////////////////
// General PCIe driver functions
///////////////////////////////////////////////////////////////////////////////

struct dct_pcie_of_data {
	enum dw_pcie_device_mode mode;
};

static const struct dct_pcie_of_data dct_pcie_of_data_rc = {
	.mode = DW_PCIE_RC_TYPE,
};

static const struct dct_pcie_of_data dct_pcie_of_data_ep = {
	.mode = DW_PCIE_EP_TYPE,
};

static const struct of_device_id dct_pcie_of_match[] = {
	{
		.compatible = "dct,pcie-rc",
		.data = &dct_pcie_of_data_rc,
	},
	{
		.compatible = "dct,pcie-ep",
		.data = &dct_pcie_of_data_ep,
	},
	{},
};
MODULE_DEVICE_TABLE(of, dct_pcie_of_match);

static int dct_pcie_probe(struct platform_device *pdev)
{
	int ret;
	int irq_ctl;
	int irq_err;

	void __iomem *base;
	void __iomem *dbi_base;
	struct dw_pcie *dw_pcie;
	struct dct_pcie *dct_pcie;
	struct device *dev = &pdev->dev;
	struct device_node *devnode = dev->of_node;

	const struct dct_pcie_of_data *data;

	struct dct_pcie_gpio_s *gpio;

	uint32_t regval; 

	int i;

	DCT_PCIE_TRACE_LOG("ENTER");

	data = of_device_get_match_data(dev);
	if (!data)
		return -EINVAL;

	dct_pcie = devm_kzalloc(dev, sizeof(*dct_pcie), GFP_KERNEL);
	if (!dct_pcie)
		return -ENOMEM;

	dw_pcie = devm_kzalloc(dev, sizeof(*dw_pcie), GFP_KERNEL);
	if (!dw_pcie)
		return -ENOMEM;

	dw_pcie->dev = dev;

	dct_pcie->mode = (enum dw_pcie_device_mode)data->mode;
	if (dct_pcie->mode == DW_PCIE_RC_TYPE) {
		dev_info(dev, "device type PCIE_RC\n");
		dw_pcie->ops = &dct_pcie_rc_link_ops;
	} else if (dct_pcie->mode == DW_PCIE_EP_TYPE) {
		dev_info(dev, "device type PCIE_EP\n");
		dw_pcie->ops = &dct_pcie_ep_link_ops;
	} else {
		dev_err(dev, "INVALID device type %d\n", dct_pcie->mode);
		return -EINVAL;
    }

	irq_ctl = platform_get_irq_byname(pdev, "ctl");
	if (irq_ctl < 0)
		return irq_ctl;

	irq_err = platform_get_irq_byname(pdev, "error");
	if (irq_err < 0)
		return irq_err;

	DCT_PCIE_TRACE_LOG("get PCIe core irq");
	DCT_PCIE_TRACE_LOG("  ctl:  %d", irq_ctl);
	DCT_PCIE_TRACE_LOG("  err:  %d", irq_err);

	base = devm_platform_ioremap_resource_byname(pdev, "ctl");
	if (IS_ERR(base))
		return PTR_ERR(base);

	dbi_base = devm_platform_ioremap_resource_byname(pdev, "dbi");
	if (IS_ERR(dbi_base))
		return PTR_ERR(dbi_base);

	dw_pcie->atu_base = devm_platform_ioremap_resource_byname(pdev, "atu");
	if (IS_ERR(dw_pcie->atu_base))
		return PTR_ERR(dw_pcie->atu_base);

#if defined(DCT_PCIE_TRACE)
	{
		struct resource *res;
		res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "ctl");
		if (res) {
			DCT_PCIE_TRACE_LOG("get ctl register from dt");
			DCT_PCIE_TRACE_LOG("  base: 0x%016llx / 0x%08llx", res->start, resource_size(res));
			DCT_PCIE_TRACE_LOG("  ptr:  0x%016lx", (uintptr_t)base);
		}
		res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "dbi");
		if (res) {
			DCT_PCIE_TRACE_LOG("get dbi register from dt");
			DCT_PCIE_TRACE_LOG("  base: 0x%016llx / 0x%08llx", res->start, resource_size(res));
			DCT_PCIE_TRACE_LOG("  ptr:  0x%016lx", (uintptr_t)dbi_base);
		}
		res = platform_get_resource_byname(pdev, IORESOURCE_MEM, "atu");
		if (res) {
			DCT_PCIE_TRACE_LOG("get atu register from dt");
			DCT_PCIE_TRACE_LOG("  base: 0x%016llx / 0x%08llx", res->start, resource_size(res));
			DCT_PCIE_TRACE_LOG("  ptr:  0x%016lx", (uintptr_t)dw_pcie->atu_base);
		}
	}
#endif

	if (of_property_read_u32(devnode, "max-link-speed", &dct_pcie->link.max_gen) != 0){
		dev_warn(dev, "Missing *max-link-speed* property in device tree\n");
		dct_pcie->link.max_gen = 1;
	}
	if (dct_pcie->link.max_gen < 1 || dct_pcie->link.max_gen > 3) {
		dev_err(dev, "Invalid *max-link-speed* property\n");
		return -EINVAL;
	}

	if (of_property_read_u32(devnode, "num-lanes", &dct_pcie->link.num_lanes) != 0){
		dev_warn(dev, "Missing *num-lanes* property in device tree\n");
		dct_pcie->link.num_lanes = 1;
	}
	if (dct_pcie->link.num_lanes < 1 || dct_pcie->link.num_lanes > 4) {
		dev_err(dev, "Invalid *num-lanes* property\n");
		return -EINVAL;
	}

	dct_pcie->link.status = DCT_PCIE_LINK_DISABLED;

	DCT_PCIE_TRACE_LOG("get PCIe core config");
	DCT_PCIE_TRACE_LOG("  max gen: %d", dct_pcie->link.max_gen);
	DCT_PCIE_TRACE_LOG("  lanes:   %d", dct_pcie->link.num_lanes);

	dct_pcie->dw_pcie = dw_pcie;
	dct_pcie->dw_pcie->dbi_base = dbi_base;
	dct_pcie->dbi_base = dbi_base;
	dct_pcie->base = base;

	// setup GPIOs
	gpio = &dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn];
	gpio->name = "pcie-perstn";
	if (dct_pcie->mode == DW_PCIE_RC_TYPE) {
		gpio->mode = GPIOD_OUT_LOW;
	} else {
		gpio->mode = GPIOD_IN;
	}
	gpio->desc = devm_gpiod_get(dev, gpio->name, gpio->mode);
	if (IS_ERR(gpio->desc)) {
		dev_err(dev, "failed to retrieve %s pin\n", gpio->name);
		return -1;
	}
	if (dct_pcie->mode == DW_PCIE_RC_TYPE) {
		gpio->irq = -1;
		gpiod_set_value_cansleep(gpio->desc, 0);
	} else {
		gpio->irq = gpiod_to_irq(gpio->desc);
	}

	gpio = &dct_pcie->gpio[DCT_PCIE_GPIO_CLKREQn];
	gpio->name = "pcie-clkreqn";
	gpio->mode = GPIOD_IN;
	gpio->desc = devm_gpiod_get(dev, gpio->name, gpio->mode);
	if (IS_ERR(gpio->desc)) {
		dev_err(dev, "failed to retrieve %s pin\n", gpio->name);
		return -1;
	}
	gpio->irq = -1;

	gpio = &dct_pcie->gpio[DCT_PCIE_GPIO_PEWAKEn];
	gpio->name = "pcie-waken";
	gpio->mode = GPIOD_IN;
	gpio->desc = devm_gpiod_get(dev, gpio->name, gpio->mode);
	if (IS_ERR(gpio->desc)) {
		dev_err(dev, "failed to retrieve %s pin\n", gpio->name);
		return -1;
	}
	gpio->irq = -1;

	DCT_PCIE_TRACE_LOG("get PCIe gpio config");
	for (gpio = dct_pcie->gpio, i = 0; i < DCT_PCIE_GPIO_COUNT; i++, gpio++) {
		DCT_PCIE_TRACE_LOG("  GPIO %d - %s : %s : IRQ %d %s", i, gpio->name, (gpio->mode == GPIOD_IN) ? "IN" : (gpio->mode == GPIOD_OUT_LOW) ? "OUT_LOW" : (gpio->mode == GPIOD_OUT_HIGH) ? "OUT_HIGH" : "UNKNOWN", gpio->irq, (gpio->irq < 0) ? "disabled" : "enabled" );
	}

	// set up PCIe core interrupts
	ret = devm_request_irq(dev, irq_ctl, dct_pcie_core_irq_ctl_handler, IRQF_SHARED, "dct_pcie-pcie-irq-ctl", dct_pcie);
	if (ret) {
		dev_err(dev, "failed to request irq_ctl\n");
		return ret;
	}

	ret = devm_request_irq(dev, irq_err, dct_pcie_core_irq_err_handler, IRQF_SHARED, "dct_pcie-pcie-irq-err", dct_pcie);
	if (ret) {
		dev_err(dev, "failed to request irq_err\n");
		return ret;
	}

	platform_set_drvdata(pdev, dct_pcie);

#ifdef DCT_PCIE_TRACE_LTSSM_INTERVAL_MS
	DCT_PCIE_TRACE_LTSSM_LOG("LTSSM Monitor Worker Thread - Init");
	dct_pcie->ltssm_monitor.workqueue = alloc_workqueue("dct_pcie_ltssm_monitor_workqueue", WQ_MEM_RECLAIM | WQ_HIGHPRI, 0);
	if (!dct_pcie->ltssm_monitor.workqueue) {
		dev_err(dev, "failed to allocate the work queue\n");
		return -ENOMEM;
	}
	INIT_DELAYED_WORK(&dct_pcie->ltssm_monitor.worker, dct_pcie_ltssm_monitor_func);
#endif

	// release CTL resets
	DCT_PCIE_TRACE_LOG("release CTL resets");
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_CTL_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	// release PHY resets
	DCT_PCIE_TRACE_LOG("release PHY resets");
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_PHY_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	// release PIPE_LANE resets
	DCT_PCIE_TRACE_LOG("release PIPE_LANE resets");
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_PIPE_LANE_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	// enable DBI clock
	DCT_PCIE_TRACE_LOG("enable DBI clock");
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_EN_CLK_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_EN_CLK_AXI_PCIE_DBI);
	dct_pcie_ctl_writel(dct_pcie, PCIE_EN_CLK_REG, regval);

	// release DBI reset
	DCT_PCIE_TRACE_LOG("release DBI reset");
	regval = dct_pcie_ctl_readl(dct_pcie, PCIE_RESETS_REG);
	regval = DCT_PCIE_REGS_FLAG_SET(regval, SW_RESET_AXI_PCIE_DBI_N);
	dct_pcie_ctl_writel(dct_pcie, PCIE_RESETS_REG, regval);

	if (dct_pcie->mode == DW_PCIE_RC_TYPE) {
		// for PCIe RC finish the initialization at probe time
		ret = dct_pcie_rc_core_enable(dct_pcie, pdev);
		if (ret) {
			dev_err(dev, "failed to enable pcie rc core\n");
			return ret;
		}

	} else {
		// for PCIe EP the initialization finishes at perstn IRQ
		gpio = &dct_pcie->gpio[DCT_PCIE_GPIO_PERSTn];

		irq_set_status_flags(gpio->irq, IRQ_NOAUTOEN);
		ret = devm_request_threaded_irq(dev, gpio->irq, NULL, dct_pcie_core_ep_irq_perst_handler, IRQF_TRIGGER_RISING | IRQF_ONESHOT, "dct_pcie-pcie-perstn", dct_pcie);
		if (ret) {
			dev_err(dev, "Failed to request irq_perst\n");
			return ret;
		}

		ret = dct_pcie_ep_core_enable(dct_pcie, pdev);
		if (ret) {
			dev_err(dev, "failed to enable pcie rc core\n");
			return ret;
		}
	}

	return 0;

}

static void dct_pcie_shutdown(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct dct_pcie *dct_pcie = dev_get_drvdata(dev);
	int ret;

	DCT_PCIE_TRACE_LOG("ENTER");

	if (dct_pcie->mode == DW_PCIE_RC_TYPE) {
		dct_pcie_rc_ops_link_stop(dct_pcie->dw_pcie);
	} else {
		dct_pcie_link_stop(dct_pcie);
	}
}

static void dct_pcie_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct dct_pcie *dct_pcie = dev_get_drvdata(dev);

	DCT_PCIE_TRACE_LOG("ENTER");

	if (dct_pcie->mode == DW_PCIE_RC_TYPE) {
		dct_pcie_rc_core_disable(dct_pcie, pdev);
	} else {
		dct_pcie_ep_core_disable(dct_pcie, pdev);
	}
}

static struct platform_driver dct_pcie_driver = {
    .probe = dct_pcie_probe,
    .remove_new = dct_pcie_remove,
    .shutdown = dct_pcie_shutdown,
    .driver =
        {
            .name = "dct_pcie",
            //.owner = THIS_MODULE,
            .of_match_table = dct_pcie_of_match,
			//.suppress_bind_attrs = true,
        },
};
module_platform_driver(dct_pcie_driver);

MODULE_DESCRIPTION("DCT PCIe controller kernel module driver");
MODULE_AUTHOR("Sven Himstedt <sven.himstedt@dreamchip.de>");
MODULE_LICENSE("GPL v2");
